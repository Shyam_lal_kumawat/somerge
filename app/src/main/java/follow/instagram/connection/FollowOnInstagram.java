package follow.instagram.connection;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import Fragment.OtherAccount;
import utils.Common;
import utils.Utils;

/**
 * Created by user7 on 2/12/16.
 */


public class FollowOnInstagram extends AsyncTask<Void, Void, String> {


    private static final String API_URL = "https://api.instagram.com/v1";
    private static final String TAG = "InstagramAPI";
    private final OtherAccount otherAccountFragment;

    private Context mctx;
    private String otherId;
    private String token;
    public static ProgressDialog mProgress = null;
    public Boolean followSuces;

    public FollowOnInstagram(OtherAccount otherAccountFragment, Context context, String otheriInstaid, String token) {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        if (mProgress != null) {
            mProgress = null;
        }
        mProgress = new ProgressDialog(context);
        this.mctx = context;
        this.otherId = otheriInstaid;

        this.token = token;
        this.otherAccountFragment = otherAccountFragment;

    }


    @Override
    protected void onPreExecute() {


        if (mProgress != null) {
            mProgress.setMessage("Loading ...");
            mProgress.show();
        }

    }


    @Override
    protected String doInBackground(Void... voids) {

        String response = null;

        Log.i(TAG, "Fetching Follow info");
        try {

            URL url = new URL(API_URL + "/users/" + otherId + "/relationship"
                    + "/?access_token=" + token);

            // URL url = new URL(mTokenUrl + "&code=" + code);
            Log.i(TAG, "Opening Token URL " + url.toString());
            HttpURLConnection urlConnection = (HttpURLConnection) url
                    .openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            // urlConnection.connect();
            OutputStreamWriter writer = new OutputStreamWriter(
                    urlConnection.getOutputStream());
            writer.write("action=follow");
            writer.flush();
            response = Utils.streamToString(urlConnection
                    .getInputStream());
            if (response != null) {

                Log.i(TAG, "response " + response);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (response != null) {
            return response.toString();
            //Log.i(TAG, "response " + response);
        } else {
            return "";

        }

    }


    @Override
    protected void onPostExecute(String response) {


        JSONObject jsonObj = null;

        try {

            if (mProgress != null) {
                if (mProgress.isShowing()) {
                    mProgress.dismiss();
                    mProgress.cancel();
                }
                // mProgress = null;
            }

            if (!response.equals("")) {


                jsonObj = new JSONObject(response);
                JSONObject innerjson = jsonObj.getJSONObject("data");

                final String outgoing_status = innerjson.getString("outgoing_status");
                String incoming_status = innerjson.getString("incoming_status");
                final Boolean target_user_is_private = innerjson.getBoolean("target_user_is_private");

                if (!target_user_is_private) {

                    if (outgoing_status.equals("follows")) {

                        followSuces = true;

                        Common.showToast(mctx, "You are now following ");

                        otherAccountFragment.disableBtn(followSuces);

                    } else if (outgoing_status.equals("requested")) {
                        followSuces = true;
                        Common.showToast(mctx, " Your follow request sucessfully sent");
                        otherAccountFragment.disableBtn(followSuces);

                    } else if (outgoing_status.equals("none")) {
                        followSuces = false;

                        Common.showToast(mctx, "Some Error occured!. Please try again");

                        otherAccountFragment.disableBtn(followSuces);
                    }
                } else {
                    followSuces = false;
                    Common.showToast(mctx, "This has private account on Instagram!");
                    otherAccountFragment.disableBtn(followSuces);
                }

            } else {
                Common.showToast(mctx, "Authentication Error");
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


}



