package request;


import com.somerge.BuildConfig;

/**
 * Created by user4 on 31/5/16.
 */
public class Constant {

    public static final int SERVICE_TYPE_GET = 1;
    public static final int SERVICE_TYPE_POST = 2;
    public static final String imageBaseUrl = "http://employeetracker.us/somerge/";


    public static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 0;
    public static final int GALLERY_IMAGE_ACTIVITY_REQUEST_CODE = 1;
    public static final int CROP_IMAGE_ACTIVITY_REQUEST_CODE = 2;


    public static class Config {
        public static final boolean DEVELOPER_MODE = BuildConfig.DEBUG;
    }

}
