package request;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;


/**
 * Created by Asus on 13-07-2015.
 */
public interface RetroDataRequest {


    @FormUrlEncoded
    @POST("somerge/services/{path1}")
    public Call<String> dataRequest(@Path("path1") String path1, @QueryMap(encoded = true) Map<String, String> action, @FieldMap(encoded = true) Map<String, String> fieldValue);

    @Multipart
    @POST("somerge/services/{path1}")
    public Call<String> dataRequestMultiPart(@Path("path1") String path1, @QueryMap(encoded = true) Map<String, String> action, @PartMap Map<String, RequestBody> fieldValue, @Part MultipartBody.Part files);

    @GET("somerge/services/{path1}")
    public Call<String> dataRequestGet(@Path("path1") String path1, @QueryMap(encoded = true) Map<String, String> action);
}
