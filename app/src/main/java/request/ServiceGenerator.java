package request;


import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;


/**
 * Created by Asus on 13-07-2015.
 */
public class ServiceGenerator {
   // private static RestAdapter.Builder builder = new RestAdapter.Builder().setClient(new OkClient());

    // No need to instantiate this class.
    private ServiceGenerator() {
    }


    public static <S> S createService(Class<S> serviceClass, String baseUrl) {

// set your desired log level
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(10, TimeUnit.MINUTES);
        httpClient.readTimeout(10, TimeUnit.MINUTES);

        Retrofit retrofit   =new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .baseUrl(baseUrl).build();
        return retrofit.create(serviceClass);
    }



}
