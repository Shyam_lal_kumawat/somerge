package request;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.somerge.R;
import com.somerge.SignUpActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.Common;
import utils.Log;
import utils.SignatureCommonMenthods;


public class ServerRequest {

    private Context context;
    private RequestedServiceDataModel requestedServiceDataModel;
    private Type responseClass = null;
    private Dialog progressdialog;


    public ServerRequest(Context context, RequestedServiceDataModel serviceRequests) {

        this.context = context;
        this.requestedServiceDataModel = serviceRequests;
        progressdialog = new Dialog(context);
        progressdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressdialog.setContentView(R.layout.view_progress);
        progressdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressdialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

    }

    public void executeRequest() throws UnsupportedEncodingException, JSONException {

        showProgressBar();
        progressStart(requestedServiceDataModel.getBaseRequestData());
        RetroDataRequest userService;
        String Url = "http://employeetracker.us/";
//        String Url = "http://192.168.0.32/";

        userService = ServiceGenerator.createService(RetroDataRequest.class, Url);


        showProgressBar();
        Call<String> call = null;
        if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET) {
            call = userService.dataRequestGet(requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction());
            System.out.print("url" + call.request().url().toString());
        } else if (requestedServiceDataModel.getFile() == null) {

            Log.log("data is", requestedServiceDataModel.getQurry() + "");

            call = userService.dataRequest(requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction(), requestedServiceDataModel.getQurry());
        } else {
            Log.log("data is", requestedServiceDataModel.getQurry() + "  file is " + requestedServiceDataModel.getFile() + "");
            call = userService.dataRequestMultiPart(requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction(), getMultiPartDataText(), getMultiPartDataImage());
        }

        System.out.println("url" + call.request().url().toString());

        android.util.Log.v("url is", call.request().url().toString());

        call.enqueue(stringCallback);


    }

    public void executeRequestWithoutProgressbar() throws UnsupportedEncodingException, JSONException {

//        showProgressBar();
        progressStart(requestedServiceDataModel.getBaseRequestData());
        RetroDataRequest userService;
        String Url = "http://employeetracker.us";
//        String Url = "http://192.168.0.32/";

        userService = ServiceGenerator.createService(RetroDataRequest.class, Url);
        Call<String> call = null;
        if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET) {
            call = userService.dataRequestGet(requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction());
        } else if (requestedServiceDataModel.getFile() == null) {
            Log.log("data is", requestedServiceDataModel.getQurry() + "");
            call = userService.dataRequest(requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction(), requestedServiceDataModel.getQurry());
        } else {
            call = userService.dataRequestMultiPart(requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction(), getMultiPartDataText(), getMultiPartDataImage());
        }
        System.out.print("url" + call.request().url().toString());
        call.enqueue(stringCallback);

    }

    private void showProgressBar() {

        /*CharSequence title = context.getResources().getString(R.string.app_name);
        CharSequence msg = context.getResources().getString(R.string.loading);*/

        // progressdialog = new TransparentProgressDialog(context);
        progressdialog.setCancelable(false);
        progressdialog.show();
    }

    private void progressStart(BaseRequestData requestData) {


    }

    private void progressEnd(BaseRequestData requestData) {
        if (progressdialog.isShowing()) {
            try {
                progressdialog.dismiss();
            } catch (Exception e) {

            }

        }
    }

    Callback<String> stringCallback = new Callback<String>() {
        private String s;
        private Response response;

        @Override
        public void onResponse(Call<String> s, Response<String> response) {

            progressEnd(requestedServiceDataModel.getBaseRequestData());
//            Log.log(response.body());

//            Log.log("response for service", response.body() + "");


            try {
                System.out.println("response?????" + response.body());
                JSONObject jsonObject = new JSONObject(response.body());
                String status = jsonObject.getString("status");
                String message = jsonObject.getString("msg");


                if (status.equalsIgnoreCase("true")) {
                    JSONObject jsonData = null;

//                    Log.log("user id",jsonObject.getString("userid"));

                    if (jsonObject.has("userid")) {

                        Common.SetPreferences(context, "userid", jsonObject.getString("userid"));
                    }


                    if (jsonObject.has("data")) {
                        if (jsonObject.getJSONObject("data") != null) {
                            jsonData = jsonObject.getJSONObject("data");
                            if (requestedServiceDataModel.getResponseDelegate() != null) {
                                requestedServiceDataModel.getResponseDelegate().onSuccess(jsonData.toString(), message, requestedServiceDataModel.getBaseRequestData());
                            }
                        }
                    } else {
                        requestedServiceDataModel.getResponseDelegate().onSuccess("", message, requestedServiceDataModel.getBaseRequestData());
                    }


                } else if (status.equals("deactive")) {
                    Common.showToast(context, message);
                    if (message.equalsIgnoreCase("Your account is deactivated by admin team for review")) {

//                        LoginManager.getInstance().logOut();
                        context.getSharedPreferences("prefs_login", Activity.MODE_PRIVATE).edit().clear().commit();
                        Intent intent = new Intent(context, SignUpActivity.class);
                        intent.putExtra("type", "delete");

                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intent);
                    }

                } else {

                    if (jsonObject.has("userid")) {

                        Common.SetPreferences(context, "userid", jsonObject.getString("userid"));
                    }

//                    requestedServiceDataModel.getResponseDelegate().onFailure(message, requestedServiceDataModel.getBaseRequestData());
                    JSONObject jsonData = null;
                    if (jsonObject.has("data")) {
                        if (jsonObject.getJSONObject("data") != null) {
                            jsonData = jsonObject.getJSONObject("data");
                            if (requestedServiceDataModel.getResponseDelegate() != null) {
                                requestedServiceDataModel.getResponseDelegate().onFailure(jsonData.toString(), message, requestedServiceDataModel.getBaseRequestData());
                            }
                        } else {
                            Common.showToast(context, message);
                        }
                    } else {
                        requestedServiceDataModel.getResponseDelegate().onFailure("", message, requestedServiceDataModel.getBaseRequestData());
                    }

//                    if (message.equalsIgnoreCase("Your account is deactivated by admin team for review")) {
//
//                        LoginManager.getInstance().logOut();
//                        context.getSharedPreferences("prefs_login", Activity.MODE_PRIVATE).edit().clear().commit();
//                        Intent intent = new Intent(context, SignUpActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        context.startActivity(intent);


//                        boolean is_xammp_running = isServiceRunning(YatchMeConnectionService.class);
//                        if (is_xammp_running == true) {
//                            context.stopService(new Intent(context, YatchMeConnectionService.class));
//                        }

//                        context.getSharedPreferences("prefs_login", Activity.MODE_PRIVATE).edit().clear().commit();
//                        requestedServiceDataModel.getResponseDelegate().onNoNetwork(message, requestedServiceDataModel.getBaseRequestData());
//                        Intent intent = new Intent(context, SplashActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        context.startActivity(intent);
//                        Comman.showToast(context, message);

//                    }

//                    Comman.showToast(context, message);
                }


            } catch (JSONException e) {

                Log.log("exception is>>>>>>>>", e + "");

                e.printStackTrace();
            }


        }

        @Override
        public void onFailure(Call<String> call, Throwable t) {
            progressEnd(requestedServiceDataModel.getBaseRequestData());
            t.printStackTrace();
        }
    };


    private boolean isServiceRunning(Class<?> backgroundLocationServiceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (backgroundLocationServiceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void showToast(String msg, Context context) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();

    }

    public HashMap<String, String> getAction() {

        HashMap<String, String> qurryMap = new HashMap<>();
        ArrayList<String> strings = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();

        for (Map.Entry<String, String> stringStringEntry : requestedServiceDataModel.getQurry().entrySet()) {
            strings.add(stringStringEntry.getKey());
            if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET) {
                try {
                    jsonObject.put(stringStringEntry.getKey(), stringStringEntry.getValue());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        JSONArray jsonArray = new JSONArray();

        strings.add(requestedServiceDataModel.getWebServiceType().toLowerCase());
        qurryMap.put("signature", SignatureCommonMenthods.getSignatureForAPI(strings));
        qurryMap.put("type", requestedServiceDataModel.getWebServiceType());
        if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET) {
            jsonArray.put(jsonObject);
            qurryMap.put("data", jsonArray.toString());
            requestedServiceDataModel.setQurry(new HashMap<String, String>());
        }
        return qurryMap;
    }

    public HashMap<String, RequestBody> getMultiPartDataText() {
        HashMap<String, RequestBody> bodyHashMap = new HashMap<>();
        for (Map.Entry<String, String> stringStringEntry : requestedServiceDataModel.getQurry().entrySet()) {
            bodyHashMap.put(stringStringEntry.getKey(), RequestBodyUtils.getRequestBodyString(stringStringEntry.getValue()));
        }
        return bodyHashMap;
    }

    public MultipartBody.Part getMultiPartDataImage() {
        List<MultipartBody.Part> bodyHashMap = new ArrayList<>();
        for (Map.Entry<String, File> stringStringEntry : requestedServiceDataModel.getFile().entrySet()) {
            bodyHashMap.add(RequestBodyUtils.getRequestBodyImage(stringStringEntry.getValue(), stringStringEntry.getKey()));
        }
        if (bodyHashMap.size() > 0) {
            return bodyHashMap.get(0);
        }
        return null;
    }
}
