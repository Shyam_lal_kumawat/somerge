package request;


import android.content.Context;
import android.widget.Toast;

import utils.Common;
import utils.Utils;


public class RequestedServiceDataModel extends DataModel {

    private ServerRequest dostoomServerRequest;

    public RequestedServiceDataModel(Context context, ResponseDelegate delegate) {
        super(context, delegate);
    }

    public void execute() {

        if (Utils.isOnline(context)) {
            dostoomServerRequest = new ServerRequest(context, this);
            try {
                dostoomServerRequest.executeRequest();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else {
            if (isShowNetworkTost()) {
                Toast.makeText(context, "Please check your internet connection.", Toast.LENGTH_LONG).show();
            } else {
                if (responseDelegate != null) {
                    responseDelegate.onNoNetwork("Please check your internet connection.", getBaseRequestData());
                    Common.showToast(context, "Please check your internet connection.");

                }
            }

        }

    }

    public void executeWithoutProgressbar() {

        if (Utils.isOnline(context)) {
            dostoomServerRequest = new ServerRequest(context, this);
            try {
                dostoomServerRequest.executeRequestWithoutProgressbar();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else {
            if (isShowNetworkTost()) {
                Toast.makeText(context, "Please check your internet connection.", Toast.LENGTH_LONG).show();
            } else {
                if (responseDelegate != null) {
                    responseDelegate.onNoNetwork("Please check your internet connection.", getBaseRequestData());
                    Common.showToast(context, "Please check your internet connection.");

                }
            }
        }

    }

}
