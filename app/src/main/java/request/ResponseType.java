package request;

/**
 * Created by user4 on 31/5/16.
 */
public interface ResponseType {

    public static final int SIGNUP = 101;
    public static final int LOGIN = 102;
    public static final int CHECKUSER = 103;
    public static final int GETPROFILE = 104;
    public static final int EDITPROFILE = 105;
    public static final int LOGOUT = 106;
    public static final int CHANGE = 107;
    public static final int FORGOT = 108;
    public static final int add = 109;

    public static final int ADD_SOCIAL = 110;
    public static final int ADD_CONTACT = 111;
    public static final int SOICAL_SETTING = 112;

}
