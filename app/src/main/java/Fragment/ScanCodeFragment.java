package Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.zxing.Result;
import com.somerge.CommanFragmentActitvty;

import gettersetter.Data;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import utils.Common;
import utils.CommonMethods;

/**
 * Created by user7 on 19/10/16.
 */
public class ScanCodeFragment extends Fragment implements ZXingScannerView.ResultHandler {


    private ViewGroup parentView;
    CommonMethods commanMethods;
    private Activity _activity;
    private ZXingScannerView mScannerView;
    private Data data;
    private String userid;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        _activity = getActivity();

        Gson gson = new Gson();
        data = gson.fromJson(Common.getPreferences(_activity, "data"), Data.class);
        userid = data.getUserid();


        commanMethods = new CommonMethods(_activity, this);

        mScannerView = new ZXingScannerView(_activity);   // Programmatically initialize the scanner view


        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.


        mScannerView.startCamera();


        return mScannerView;


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        commanMethods.setUpToolbars((AppCompatActivity) _activity, "Scan Code");

    }


    @Override
    public void onPause() {
        super.onPause();
        if (mScannerView != null) {
            mScannerView.stopCamera();           // Stop camera on pause
        }

    }


    @Override
    public void handleResult(Result rawResult) {

        // Do something with the result here


        if (!rawResult.getText().equalsIgnoreCase("")) {


            if (!rawResult.getText().toString().equals(userid)) {
                OtherAccount otherAccount = new OtherAccount();


                Bundle b = new Bundle();
                b.putString("userid", rawResult.getText());

                otherAccount.setArguments(b);

                ((CommanFragmentActitvty) _activity).Loadfragment(otherAccount);

            } else {

                Common.showToast(_activity, "Can't scan yourself. Please scan again");
                _activity.onBackPressed();

             /*   mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
                mScannerView.startCamera();
*/
            }

        } else {
            _activity.onBackPressed();
            Common.showToast(_activity, "Please scan again");
        }



/*

        Log.e("handler", rawResult.getText()); // Prints scan results
        Log.e("handler", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode)


        // show the scanner result into dialog box.
        AlertDialog.Builder builder = new AlertDialog.Builder(_activity);
        builder.setTitle("Scan Result");
        builder.setMessage(rawResult.getText());
        AlertDialog alert1 = builder.create();
        alert1.show();

        // If you would like to resume scanning, call this method below:
        // mScannerView.resumeCameraPreview(this);
*/


    }


}
