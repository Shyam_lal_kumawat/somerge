package Fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.somerge.CommanFragmentActitvty;
import com.somerge.R;

import adpter.Search_users_Adapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gettersetter.Data;
import gettersetter.SearchResult;
import request.BaseRequestData;
import request.Constant;
import request.RequestedServiceDataModel;
import request.ResponseDelegate;
import request.ResponseType;
import utils.BoldTextView;
import utils.Common;
import utils.CommonMethods;
import utils.MediumEditText;

/**
 * Created by user7 on 19/10/16.
 */
public class SearchFragment extends Fragment implements ResponseDelegate {


    @Bind(R.id.img_search)
    ImageView imgSearch;
    @Bind(R.id.text_Search)
    MediumEditText textSearch;
    @Bind(R.id.ll_Search)
    LinearLayout llSearch;
    @Bind(R.id.View)
    android.view.View View;
    @Bind(R.id.ScanCode)
    BoldTextView ScanCode;
    @Bind(R.id.img_camera)
    ImageView imgCamera;
    private View parentView;
    CommonMethods commanMethods;
    private Activity _activity;
    RecyclerView recyclerView;
    private Search_users_Adapter search_users_adapter;

    private RequestedServiceDataModel requestedServiceDataModel;
    private Data data;
    private String userid;
    private SearchResult searchresult;

    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 123;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        _activity = getActivity();
        parentView = inflater.inflate(R.layout.search_users, null, false);


        commanMethods = new CommonMethods(_activity, this);

        ButterKnife.bind(this, parentView);

        setBodyUI();


        return parentView;


    }

    private void setBodyUI() {


        Gson gson = new Gson();
        data = gson.fromJson(Common.getPreferences(_activity, "data"), Data.class);
        userid = data.getUserid();

        recyclerView = (RecyclerView) parentView.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(_activity);
        recyclerView.setLayoutManager(mLayoutManager);


        textSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {

                    textSearch.setGravity(Gravity.LEFT);
                }

                return false;
            }
        });


        textSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {


                    if (Common.getConnectivityStatus(_activity)) {
                        if (!v.getText().toString().trim().equals("")) {
                            performSearch(v.getText().toString());
                        } else {
                            Common.showToast(_activity, "Please enter text!");
                        }


                    } else {
                        Common.showToast(_activity, "Please check your internet connection.");

                    }

                    return true;
                }
                return false;
            }
        });

        textSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().trim().equals("")) {

                    Log.d("SOSHYAM", charSequence.toString() + "me");
                    recyclerView.setVisibility(android.view.View.VISIBLE);

                    performSearch(charSequence.toString());
                } else {

                    Log.d("SOSHYAMNOT", charSequence.toString() + "me");

                    recyclerView.setVisibility(android.view.View.INVISIBLE);
                    //recyclerView.setAdapter(null);
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {


                // Log.d("SOSHYAMCHANGED", editable.toString()+"");

            }
        });

        if (searchresult != null) {

            textSearch.setGravity(Gravity.LEFT);
           /* search_users_adapter = new Search_users_Adapter(_activity, searchresult.getDetail());
            recyclerView.setAdapter(search_users_adapter);
*/
        }

    }

    private void performSearch(String searchkeyword) {

        requestedServiceDataModel = new RequestedServiceDataModel(_activity, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.GETPROFILE);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("keyword", searchkeyword);
        requestedServiceDataModel.setWebServiceType("search");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.executeWithoutProgressbar();


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        commanMethods.setUpToolbars((AppCompatActivity) _activity, "Search Users");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.img_search, R.id.text_Search, R.id.ll_Search, R.id.ScanCode, R.id.img_camera})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_search:
                if (Common.getConnectivityStatus(_activity)) {

                    if (!textSearch.getText().toString().trim().equals("")) {
                        performSearch(textSearch.getText().toString());
                    } else {
                        Common.showToast(_activity, "Please enter text!");
                    }

                } else {
                    Common.showToast(_activity, "Please check your internet connection.");

                }

                break;
            case R.id.text_Search:


                break;
            case R.id.ll_Search:
                break;
            case R.id.ScanCode:

                if (ActivityCompat.checkSelfPermission(_activity, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Camera permission has not been granted.

                    requestCameraPermission();

                } else {
                    ((CommanFragmentActitvty) _activity).Loadfragment(new ScanCodeFragment());

                }

                break;
            case R.id.img_camera:

                if (ActivityCompat.checkSelfPermission(_activity, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Camera permission has not been granted.

                    requestCameraPermission();

                } else {
                    ((CommanFragmentActitvty) _activity).Loadfragment(new ScanCodeFragment());

                }

                break;
        }
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String jsondata, String message, BaseRequestData baseRequestData) {

        Log.v("Search Rsponse", jsondata);

        Gson gson = new Gson();


        searchresult = gson.fromJson(jsondata, SearchResult.class);

        search_users_adapter = new Search_users_Adapter(_activity, searchresult.getDetail());
        recyclerView.setAdapter(search_users_adapter);

    }

    @Override
    public void onFailure(String jsondata, String message, BaseRequestData baseRequestData) {


        Common.showToast(_activity, message);
    }


    private void requestCameraPermission() {

        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(_activity,
                Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(_activity);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("Camera permission is necessary to scan code");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(_activity, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(_activity, new String[]{Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CAMERA);
        }
        // END_INCLUDE(camera_permission_request)
    }
}

