package Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.somerge.R;

import adpter.MyConnection_adpter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gettersetter.Data;
import gettersetter.SearchResult;
import request.BaseRequestData;
import request.Constant;
import request.RequestedServiceDataModel;
import request.ResponseDelegate;
import request.ResponseType;
import utils.Common;
import utils.CommonMethods;
import utils.MediumEditText;

/**
 * Created by user7 on 19/10/16.
 */
public class MyConnection extends Fragment implements ResponseDelegate {


    @Bind(R.id.img_search)
    ImageView imgSearch;
    @Bind(R.id.text_Search)
    MediumEditText textSearch;
    @Bind(R.id.ll_Search)
    LinearLayout llSearch;
    private View parentView;
    CommonMethods commanMethods;
    private Activity _activity;
    RecyclerView recyclerView;
    private MyConnection_adpter myadpter;
    private RequestedServiceDataModel requestedServiceDataModel;
    private Data mydata;
    private SearchResult searchresult;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        _activity = getActivity();
        parentView = inflater.inflate(R.layout.your_connections, null, false);


        commanMethods = new CommonMethods(_activity, this);
        ButterKnife.bind(this, parentView);

        setBodyUI();


        return parentView;


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        commanMethods.setUpToolbars((AppCompatActivity) _activity, "Your Connections");


    }

    private void setBodyUI() {

        Gson gson = new Gson();
        mydata = gson.fromJson(Common.getPreferences(_activity, "data"), Data.class);

        recyclerView = (RecyclerView) parentView.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(_activity);
        recyclerView.setLayoutManager(mLayoutManager);
      /*  myadpter = new MyConnection_adpter(_activity, searchresult.getDetail());
        recyclerView.setAdapter(myadpter);*/
        recyclerView.setNestedScrollingEnabled(false);

        textSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {

                    textSearch.setGravity(Gravity.LEFT);
                }

                return false;
            }
        });

        textSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (myadpter != null && searchresult.getDetail() != null) {
                    String text = charSequence.toString();
                    myadpter.filter(text);

                } else {
                    Common.showToast(_activity, "You have no connection!");

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });


        if (searchresult != null) {


            myadpter = new MyConnection_adpter(_activity, searchresult.getDetail());
            recyclerView.setAdapter(myadpter);


        } else {
            performConnection();
        }


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    private void performConnection() {

        requestedServiceDataModel = new RequestedServiceDataModel(_activity, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-connection.php");
        baseRequestData.setTag(ResponseType.GETPROFILE);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", mydata.getUserid());
        requestedServiceDataModel.setWebServiceType("get");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();


    }

    @OnClick({R.id.img_search, R.id.text_Search, R.id.ll_Search})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_search:

                if (myadpter != null && searchresult.getDetail() != null) {
                    String text = textSearch.getText().toString();
                    myadpter.filter(text);

                } else {
                    Common.showToast(_activity, "You have no connection!");

                }

                break;
            case R.id.text_Search:


                if (myadpter != null && searchresult.getDetail() != null) {
                    String text = textSearch.getText().toString();
                    myadpter.filter(text);

                } else {
                    Common.showToast(_activity, "You have no connection!");

                }

                break;
            case R.id.ll_Search:
                break;
        }
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String jsondata, String message, BaseRequestData baseRequestData) {

        Log.v("Search Rsponse", jsondata);


        Gson gson = new Gson();


        searchresult = gson.fromJson(jsondata, SearchResult.class);

        myadpter = new MyConnection_adpter(_activity, searchresult.getDetail());
        recyclerView.setAdapter(myadpter);


    }

    @Override
    public void onFailure(String jsondata, String message, BaseRequestData baseRequestData) {

    }
}
