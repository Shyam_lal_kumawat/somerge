package Fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.somerge.CommanFragmentActitvty;
import com.somerge.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gettersetter.Data;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import request.Constant;
import utils.Common;
import utils.CommonMethods;
import utils.MediumTextView;

/**
 * Created by user7 on 19/10/16.
 */
public class HomeFragment extends Fragment {


    @Bind(R.id.img_pic)
    ImageView imgPic;
    @Bind(R.id.img_QR)
    ImageView imgQR;
    @Bind(R.id.your_order)
    MediumTextView yourOrder;
    @Bind(R.id.connection)
    MediumTextView connection;
    @Bind(R.id.img_camera)
    ImageView imgCamera;
    @Bind(R.id.name)
    MediumTextView name;
    @Bind(R.id.User_name)
    MediumTextView UserName;
    @Bind(R.id.progress_bar)
    ProgressBar progressBar;
    private ViewGroup parentView;
    CommonMethods commanMethods;
    private Activity _activity;
    private ZXingScannerView mScannerView;
    private Data data;

    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 123;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        _activity = getActivity();
        parentView = (ViewGroup) inflater.inflate(R.layout.landing_page, null, false);


        commanMethods = new CommonMethods(_activity, this);
        ButterKnife.bind(this, parentView);

        setBodyUI();


        return parentView;


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        commanMethods.setUpToolbars((AppCompatActivity) _activity, "SoMerge");


    }

    private void setBodyUI() {


        Gson gson = new Gson();

        data = gson.fromJson(Common.getPreferences(_activity, "data"), Data.class);

        System.out.println("Data in SHared" + data.getGenerated_image());

        name.setText(data.getFirst_name() + " " + data.getLast_name());
        UserName.setText(data.getUsername());


        Picasso.with(_activity).load(Constant.imageBaseUrl + data.getGenerated_image()).
                // memoryPolicy(MemoryPolicy.NO_CACHE)
                //.networkPolicy(NetworkPolicy.NO_CACHE).
                        placeholder(R.drawable.profile).into(imgPic, new Callback() {
            @Override
            public void onSuccess() {
                if (progressBar != null) {
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError() {
                if (progressBar != null) {
                    progressBar.setVisibility(View.GONE);
                }
                Common.showToast(_activity, "Failed to load Image! Please try again");
            }
        });
        Picasso.with(_activity).load(Constant.imageBaseUrl + data.getQr_image()).memoryPolicy(MemoryPolicy.NO_CACHE).resize(100, 100)
                .networkPolicy(NetworkPolicy.NO_CACHE).
                placeholder(R.drawable.user_circle).
                into(imgQR);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.img_pic, R.id.img_QR, R.id.your_order, R.id.connection, R.id.img_camera})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_pic:

                if (imgQR.getVisibility() == View.VISIBLE) {
                    imgQR.setVisibility(View.INVISIBLE);
                } else {
                    imgQR.setVisibility(View.VISIBLE);

                }

                break;
            case R.id.img_QR:
                break;
            case R.id.your_order:

                ((CommanFragmentActitvty) _activity).Loadfragment(new MyAccount());

                break;
            case R.id.connection:

                ((CommanFragmentActitvty) _activity).Loadfragment(new MyConnection());

                break;
            case R.id.img_camera:


                if (ActivityCompat.checkSelfPermission(_activity, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Camera permission has not been granted.

                    requestCameraPermission();

                } else {
                    ((CommanFragmentActitvty) _activity).Loadfragment(new ScanCodeFragment());

                }


                break;
        }
    }


    private void requestCameraPermission() {

        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(_activity,
                Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(_activity);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("Camera permission is necessary to scan code");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(_activity, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(_activity, new String[]{Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CAMERA);
        }
        // END_INCLUDE(camera_permission_request)
    }


}
