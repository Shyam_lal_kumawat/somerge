package Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.somerge.CommanFragmentActitvty;
import com.somerge.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import follow.instagram.connection.FollowOnInstagram;
import gettersetter.Data;
import instagram_sdk.ApplicationData;
import instagram_sdk.InstagramApp;
import io.fabric.sdk.android.Fabric;
import request.BaseRequestData;
import request.Constant;
import request.RequestedServiceDataModel;
import request.ResponseDelegate;
import request.ResponseType;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import utils.Common;
import utils.CommonMethods;
import utils.LightEditText;
import utils.LightTextView;
import utils.MediumTextView;
import utils.SemiBoldTextView;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by user7 on 19/10/16.
 */
public class OtherAccount extends Fragment implements ResponseDelegate {

    @Bind(R.id.img_profile)
    ImageView imgProfile;
    @Bind(R.id.progress_bar)
    ProgressBar progressBar;
    @Bind(R.id.img_qrcode)
    ImageView imgQrcode;
    @Bind(R.id.General)
    SemiBoldTextView General;
    @Bind(R.id.Name)
    LightTextView Name;
    @Bind(R.id.etName)
    LightEditText etName;
    @Bind(R.id.View)
    android.view.View View;
    @Bind(R.id.User_name)
    LightTextView UserName;
    @Bind(R.id.et_username)
    LightEditText etUsername;
    @Bind(R.id.Phone)
    LightTextView Phone;
    @Bind(R.id.et_phone)
    LightEditText etPhone;
    @Bind(R.id.Email)
    LightTextView Email;
    @Bind(R.id.et_email)
    LightEditText etEmail;
    @Bind(R.id.Account)
    TextView Account;
    @Bind(R.id.Set_Profile)
    MediumTextView SetProfile;
    @Bind(R.id.facebook)
    MediumTextView etfacebook;
    @Bind(R.id.rl_facebook)
    RelativeLayout rlFacebook;
    @Bind(R.id.Instagram)
    MediumTextView etInstagram;
    @Bind(R.id.rl_instagram)
    RelativeLayout rlInstagram;
    @Bind(R.id.Twitter)
    MediumTextView etTwitter;
    @Bind(R.id.rl_twitter)
    RelativeLayout rlTwitter;
    @Bind(R.id.Snapchat)
    MediumTextView etSnapchat;
    @Bind(R.id.rl_snapchat)
    RelativeLayout rlSnapchat;

    private View parentView;
    CommonMethods commanMethods;
    private static Activity _activity;

    private boolean isFB, isInsta, isTwitter, isSnap, is_Profile;
    private RequestedServiceDataModel requestedServiceDataModel;
    private String otheruserid;

    ImageView img_left, img_right;
    SemiBoldTextView middlename, left_text;
    private Dialog dialog;

    private int isClickFb = 0, isClickInsta = 0, isClickTwitter = 0, isClickSnap = 0;
    private Data mydata;

    private CallbackManager callbackManager;
    private Data otherUserdata;
    private String otherFBID;
    private String otheTwitterID;

    private TwitterAuthClient authClient;

    private static final String TWITTER_KEY = "hqK7xdX9oEqyKI7fpoORQfx5n"; // "vQhEDS9JOYysgQFhU3XggA3NY";
    private static final String TWITTER_SECRET = "6OgpGYiD2T9NK4uhXmshirwcxgeDbw48i6nTVp0tPdg1XKxTcN";//"veNWQKmGUl
    private TwitterSession CurrentTwitterSeesion;
    private InstagramApp mApp;
    private String otherINSTAID;
    private static final String API_URL = "https://api.instagram.com/v1";
    private static final String TAG = "InstagramAPI";
    private int responsecode;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        _activity = getActivity();


        parentView = inflater.inflate(R.layout.otheraccount, null, false);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(_activity);
        FacebookSdk.sdkInitialize(_activity);


        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(getActivity(), new Twitter(authConfig));

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mApp = new InstagramApp(_activity, ApplicationData.CLIENT_ID,
                ApplicationData.CLIENT_SECRET, ApplicationData.CALLBACK_URL);


        callbackManager = CallbackManager.Factory.create();


        commanMethods = new CommonMethods(_activity, this);


        ButterKnife.bind(this, parentView);
        setBodyUI();

        return parentView;


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        Bundle b = getArguments();
        otheruserid = b.getString("userid");

        Gson gson = new Gson();

        mydata = gson.fromJson(Common.getPreferences(_activity, "data"), Data.class);


        if (Common.getConnectivityStatus(_activity)) {
            serverRequestForGetProfile(otheruserid);
        } else {
            Common.showToast(_activity, "Please check your internet connection.");

        }


        //commanMethods.setUpToolbars((AppCompatActivity) _activity, "My Account");

    }

    private void setToolBar(String username) {

        Toolbar toolbar = (Toolbar) ((CommanFragmentActitvty) _activity).findViewById(R.id.toolbar);

        ((CommanFragmentActitvty) _activity).setSupportActionBar(toolbar);
        ((CommanFragmentActitvty) _activity).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((CommanFragmentActitvty) _activity).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((CommanFragmentActitvty) _activity).getSupportActionBar().setHomeButtonEnabled(true);


        toolbar.setVisibility(View.VISIBLE);


        img_left = (ImageView) toolbar.findViewById(R.id.img_left);
        middlename = (SemiBoldTextView) toolbar.findViewById(R.id.middlename);
        img_right = (ImageView) toolbar.findViewById(R.id.img_right);


        middlename.setText(username);
        img_left.setVisibility(View.VISIBLE);
        img_right.setVisibility(View.INVISIBLE);
    }


    private void setBodyUI() {


        mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {

            @Override
            public void onSuccess() {

                FollowOnInstagram followOnInstagram = new FollowOnInstagram(OtherAccount.this, getContext(), otherINSTAID, mApp.getTOken());
                followOnInstagram.execute();


            }

            @Override
            public void onFail(String error) {
                Toast.makeText(_activity, error, Toast.LENGTH_SHORT)
                        .show();
            }

        });

    }

    private void serverRequestForGetProfile(String userid) {
        requestedServiceDataModel = new RequestedServiceDataModel(_activity, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.GETPROFILE);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", mydata.getUserid());
        requestedServiceDataModel.putQurry("otheruserid", otheruserid);
        requestedServiceDataModel.setWebServiceType("GETPROFILE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);

        }


        Common.showToast(_activity, "Failed to load image! Please try again");

    }

    @Override
    public void onSuccess(String jsondata, String message, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GETPROFILE:

                Gson gson = new Gson();

                otherUserdata = gson.fromJson(jsondata, Data.class);

                setProfileData(otherUserdata);

                break;
            case ResponseType.ADD_CONTACT:

                Common.showToast(_activity, "Added Sucessfully");


                break;
        }
    }

    private void setProfileData(Data data) {

        setToolBar(data.getFirst_name() + " " + data.getLast_name());


        ShowContactSaveDialog(data);


        if (etEmail != null && etName != null && etPhone != null && etUsername != null) {
            etEmail.setEnabled(false);
            etName.setEnabled(false);
            etPhone.setEnabled(false);
            etUsername.setEnabled(false);

            etPhone.setText(data.getMobile());
            etName.setText(data.getFirst_name() + " " + data.getLast_name());
            etEmail.setText(data.getEmail());
            etUsername.setText(data.getUsername());

        }


        Log.v("imagess", Constant.imageBaseUrl + data.getGenerated_image());

        Picasso.with(_activity).load(Constant.imageBaseUrl + data.getGenerated_image())
                //.memoryPolicy(MemoryPolicy.NO_CACHE)
                // .resize(150, 150)
                //    networkPolicy(NetworkPolicy.NO_CACHE)
                .placeholder(R.drawable.profile).into(imgProfile, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                if (progressBar != null) {
                    progressBar.setVisibility(View.GONE);
                }

            }

            @Override
            public void onError() {
                if (progressBar != null) {
                    progressBar.setVisibility(View.GONE);
                }

                // Common.showToast(_activity, "Failed to load image! Please try again");


            }
        });

        Picasso.with(_activity).load(Constant.imageBaseUrl + data.getQr_image()).memoryPolicy(MemoryPolicy.NO_CACHE)
                .resize(100, 100).networkPolicy(NetworkPolicy.NO_CACHE).
                placeholder(R.drawable.user_circle).
                into(imgQrcode);


        //System.out.println("");
/*
        if (data.getSocial() != null) {
            for (int i = 0; i < data.getSocial().size(); i++) {
                setSocialConnection(data.getSocial().get(i).getSocial_site(), data.getSocial().get(i).getSocial_id());
            }

        }*/


        if (data.getSocial() != null && data.getSocial().size() > 0 && mydata.getSocial().size() > 0) {
            for (int i = 0; i < data.getSocial().size(); i++) {
                for (int j = 0; j < mydata.getSocial().size(); j++) {
                    if (data.getSocial().get(i).getSocial_site().equals(mydata.getSocial().get(j).getSocial_site())) {
                        Account.setVisibility(android.view.View.VISIBLE);
                        setSocialConnection(data.getSocial().get(i).getSocial_site(), data.getSocial().get(i).getSocial_id(), data.getSocial().get(i).getStatus());
                    }
                }

            }

        } else {
            Account.setVisibility(android.view.View.GONE);

        }


        if (data.getInstagram().equals("1")) {
            etInstagram.setEnabled(false);
            etInstagram.setCompoundDrawablesWithIntrinsicBounds(R.drawable.instagram, 0, R.drawable.toggle_on_new, 0);
        } else {
            etInstagram.setEnabled(true);
            etInstagram.setCompoundDrawablesWithIntrinsicBounds(R.drawable.instagram, 0, R.drawable.toogle_off_new, 0);
        }


        if (data.getTwitter().equals("1")) {
            etTwitter.setEnabled(false);
            etTwitter.setCompoundDrawablesWithIntrinsicBounds(R.drawable.twitter, 0, R.drawable.toggle_on_new, 0);
        } else {
            etTwitter.setEnabled(true);
            etTwitter.setCompoundDrawablesWithIntrinsicBounds(R.drawable.twitter, 0, R.drawable.toogle_off_new, 0);
        }

        if (data.getFacebook().equals("1")) {
            etfacebook.setEnabled(false);
            etfacebook.setCompoundDrawablesWithIntrinsicBounds(R.drawable.facebook_blue, 0, R.drawable.toggle_on_new, 0);
        } else {
            etfacebook.setEnabled(true);
            etfacebook.setCompoundDrawablesWithIntrinsicBounds(R.drawable.facebook_blue, 0, R.drawable.toogle_off_new, 0);
        }

     /*   if (data.getSocial().size() > 0) {
            showConnectionDialog(data);

        }*/


    }

    private void ShowContactSaveDialog(final Data searchdata) {


        AlertDialog myAlertDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(_activity);
        builder.setTitle("Alert");
        builder.setMessage("Do you want to add " + searchdata.getFirst_name() + " " + searchdata.getLast_name() + " in your contacts?");
        builder.setPositiveButton("Yes ", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.dismiss();


                //  DowonLoadVCF(searchdata);


                Intent intent = new Intent(Intent.ACTION_INSERT);
                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);

                intent.putExtra(ContactsContract.Intents.Insert.NAME, searchdata.getFirst_name() + " " + searchdata.getLast_name());
                intent.putExtra(ContactsContract.Intents.Insert.PHONE, searchdata.getMobile());
                intent.putExtra(ContactsContract.Intents.Insert.EMAIL, searchdata.getEmail());

                _activity.startActivity(intent);

            }
        });

        builder.setNegativeButton("No ", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.dismiss();


            }
        });

        builder.setCancelable(false);
        myAlertDialog = builder.create();
        myAlertDialog.show();

    }


    private void DowonLoadVCF(Data data) {


        File vcfFile = new File(_activity.getExternalFilesDir(null), "somerge_+" + data.getUsername() + ".vcf");
        FileWriter fw = null;
        try {
            fw = new FileWriter(vcfFile);
            fw.write("BEGIN:VCARD\r\n");
            fw.write("VERSION:3.0\r\n");
            fw.write("N:" + data.getUsername() + "\r\n");
            fw.write("FN:" + data.getFirst_name() + " " + data.getLast_name() + "\r\n");
            // fw.write("ORG:" + p.getCompanyName() + "\r\n");
            fw.write("TEL;TYPE=WORK,VOICE:" + data.getMobile() + "\r\n");
            fw.write("TEL;TYPE=HOME,VOICE:" + data.getMobile() + "\r\n");
            // fw.write("ADR;TYPE=WORK:;;" + p.getStreet() + ";" + p.getCity() + ";" + p.getState() + ";" + p.getPostcode() + ";" + p.getCountry() + "\r\n");
            fw.write("EMAIL;TYPE=PREF,INTERNET:" + data.getEmail() + "\r\n");
            fw.write("END:VCARD\r\n");
            fw.close();


            // Its for Android Nought Version that crash app due to file:// not mount directly to new version .
            //Its shows FileUriExposed Error ..

           /* Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                    vcfFile);*/

            Uri photoURI = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", vcfFile);

            Intent i = new Intent();
            i.setAction(Intent.ACTION_VIEW);
            i.setDataAndType(photoURI, "text/x-vcard");

            List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities(i, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                getActivity().grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }


            // i.setDataAndType(Uri.fromFile(vcfFile), "text/x-vcard");
            startActivity(i);


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void showConnectionDialog(final Data innerdata) {

        dialog = new Dialog(_activity, R.style.DialogSlideAnim);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.setContentView(R.layout.connection_dialog);
        _activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();


        if (isFB) {
            dialog.findViewById(R.id.rl_facebook).setVisibility(android.view.View.VISIBLE);
        } else {
            dialog.findViewById(R.id.rl_facebook).setVisibility(android.view.View.GONE);
        }

        if (isInsta) {
            dialog.findViewById(R.id.rl_Instagram).setVisibility(android.view.View.VISIBLE);
        } else {
            dialog.findViewById(R.id.rl_Instagram).setVisibility(android.view.View.GONE);
        }

        if (isTwitter) {
            dialog.findViewById(R.id.rl_twitter).setVisibility(android.view.View.VISIBLE);
        } else {
            dialog.findViewById(R.id.rl_twitter).setVisibility(android.view.View.GONE);
        }


        if (isSnap) {
            dialog.findViewById(R.id.rl_snapchat).setVisibility(android.view.View.VISIBLE);
        } else {
            dialog.findViewById(R.id.rl_snapchat).setVisibility(android.view.View.GONE);
        }
        dialog.findViewById(R.id.img_fb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (isClickFb == 0) {
                    isClickFb = 1;

                    ((ImageView) dialog.findViewById(R.id.img_fb)).setImageDrawable(_activity.getResources().getDrawable(R.drawable.tick));

                } else {
                    isClickFb = 0;
                    ((ImageView) dialog.findViewById(R.id.img_fb)).setImageDrawable(_activity.getResources().getDrawable(R.drawable.circle));
                }


            }
        });


        dialog.findViewById(R.id.img_Instagram).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isClickInsta == 0) {
                    isClickInsta = 1;

                    ((ImageView) dialog.findViewById(R.id.img_Instagram)).setImageDrawable(_activity.getResources().getDrawable(R.drawable.tick));

                } else {
                    isClickInsta = 0;
                    ((ImageView) dialog.findViewById(R.id.img_Instagram)).setImageDrawable(_activity.getResources().getDrawable(R.drawable.circle));
                }

            }
        });


        dialog.findViewById(R.id.img_Twitter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isClickTwitter == 0) {
                    isClickTwitter = 1;

                    ((ImageView) dialog.findViewById(R.id.img_Twitter)).setImageDrawable(_activity.getResources().getDrawable(R.drawable.tick));

                } else {
                    isClickTwitter = 0;
                    ((ImageView) dialog.findViewById(R.id.img_Twitter)).setImageDrawable(_activity.getResources().getDrawable(R.drawable.circle));
                }


            }
        });
        dialog.findViewById(R.id.img_Snapchat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isClickSnap == 0) {
                    isClickSnap = 1;

                    ((ImageView) dialog.findViewById(R.id.img_Twitter)).setImageDrawable(_activity.getResources().getDrawable(R.drawable.tick));

                } else {
                    isClickSnap = 0;
                    ((ImageView) dialog.findViewById(R.id.img_Twitter)).setImageDrawable(_activity.getResources().getDrawable(R.drawable.circle));
                }


            }
        });

        dialog.findViewById(R.id.Add_to_Contacts).setOnClickListener(new View.OnClickListener() {
                                                                         @Override
                                                                         public void onClick(View view) {

                                                                             dialog.dismiss();

                                                                             Log.v("isClickFb", isClickFb + "");
                                                                             Log.v("isClickInsta", isClickInsta + "");
                                                                             Log.v("isClickTwitter", isClickTwitter + "");

                                                                             //sendFriendRequest(otherFBID);

                                                                             //   authorizeTwitter();

                                                                             //    FollowOnInstagram(otherINSTAID);


                                                                            /* BaseRequestData baseRequestData = new BaseRequestData();
                                                                             baseRequestData.setWebservice("ws-connection.php");
                                                                             baseRequestData.setTag(ResponseType.ADD_CONTACT);
                                                                             baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
                                                                             requestedServiceDataModel.putQurry("userid", mydata.getUserid());
                                                                             requestedServiceDataModel.putQurry("otherid", innerdata.getUserid());
                                                                             requestedServiceDataModel.putQurry("fb", String.valueOf(isClickFb));
                                                                             requestedServiceDataModel.putQurry("insta", String.valueOf(isClickInsta));
                                                                             requestedServiceDataModel.putQurry("twitter", String.valueOf(isClickTwitter));
                                                                             requestedServiceDataModel.putQurry("snapchat", String.valueOf(isClickSnap));
                                                                             requestedServiceDataModel.setWebServiceType("add");
                                                                             requestedServiceDataModel.setBaseRequestData(baseRequestData);
                                                                             requestedServiceDataModel.execute();*/
                                                                         }


                                                                     }

        );



       /* if (innerdata.getSocial() != null) {
            for (int i = 0; i < innerdata.getSocial().size(); i++) {
                setDialogRow(innerdata.getSocial().get(i).getSocial_site());
            }

        }
*/
    }

    private void FollowOnInstagram(String otheriInstaid) {

        if (mApp.hasAccessToken()) {


            //  followUser(otheriInstaid);

            FollowOnInstagram followOnInstagram = new FollowOnInstagram(this, getContext(), otheriInstaid, mApp.getTOken());
            followOnInstagram.execute();


        } else {
            mApp.authorize();


        }
    }


    private Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == InstagramApp.WHAT_FINALIZE) {


            } else if (msg.what == InstagramApp.WHAT_FINALIZE) {
                Toast.makeText(_activity, "Check your network.",
                        Toast.LENGTH_SHORT).show();
            }
            return false;
        }
    });


    private void setSocialConnection(String social_site, String social_id, String socialStatus) {
        switch (social_site.toUpperCase()) {
            case "FACEBOOK":
                //check that user have enable FACEBOOK privacy to on so that user can request me .......
                if (socialStatus.equals("1")) {
                    etfacebook.setEnabled(true);

                    rlFacebook.setVisibility(android.view.View.VISIBLE);

                    isFB = true;


                    otherFBID = social_id;
                }

                break;

            case "INSTAGRAM":
                //check that user have enable INSTAGRAM privacy to on so that user can request me .......
                if (socialStatus.equals("1")) {
                    rlInstagram.setVisibility(android.view.View.VISIBLE);


                    isInsta = true;
                    otherINSTAID = social_id;
                }


               /* if (soicalstatus.equals("1")) {
                    etInstagram.setEnabled(false);
                    etInstagram.setCompoundDrawablesWithIntrinsicBounds(R.drawable.instagram, 0, R.drawable.toggle_on_new, 0);
                } else {
                    etInstagram.setEnabled(true);
                    etInstagram.setCompoundDrawablesWithIntrinsicBounds(R.drawable.instagram, 0, R.drawable.toogle_off_new, 0);
                }*/


                break;
            case "TWITTER":
                //check that user have enable TWITTER privacy to on so that user can request me .......:
                if (socialStatus.equals("1")) {

                    rlTwitter.setVisibility(android.view.View.VISIBLE);


                    isTwitter = true;

                    //etTwitter.setCompoundDrawablesWithIntrinsicBounds(R.drawable.twitter, 0, R.drawable.toggle_on_new, 0);
                    otheTwitterID = social_id;

                }
/*
                if (soicalstatus.equals("1")) {
                    etTwitter.setEnabled(false);
                    etTwitter.setCompoundDrawablesWithIntrinsicBounds(R.drawable.twitter, 0, R.drawable.toggle_on_new, 0);
                } else {
                    etTwitter.setEnabled(true);
                    etTwitter.setCompoundDrawablesWithIntrinsicBounds(R.drawable.twitter, 0, R.drawable.toogle_off_new, 0);
                }*/

                break;
            case "SNAPCHAT":
                etSnapchat.setEnabled(true);
                rlSnapchat.setVisibility(android.view.View.VISIBLE);


                isSnap = true;

                // etSnapchat.setCompoundDrawablesWithIntrinsicBounds(R.drawable.snapchat, 0, R.drawable.toggle_on_new, 0);


                break;
        }
    }

    @Override
    public void onFailure(String jsondata, String message, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GETPROFILE:

                Common.showToast(_activity, message);
                break;
        }
    }

    @OnClick({R.id.img_profile, R.id.img_qrcode, R.id.facebook, R.id.Instagram, R.id.Twitter, R.id.Snapchat})
    public void onClick(android.view.View view) {
        switch (view.getId()) {

            case R.id.img_profile:

                if (imgQrcode.getVisibility() == android.view.View.VISIBLE) {
                    imgQrcode.setVisibility(android.view.View.INVISIBLE);
                } else {
                    imgQrcode.setVisibility(android.view.View.VISIBLE);

                }
                break;
            case R.id.facebook:


                if (isClickFb == 0) {

                    responsecode = 1001;
                    sendFriendRequest(otherFBID);
                    isClickFb = 1;

                    etfacebook.setCompoundDrawablesWithIntrinsicBounds(R.drawable.facebook_blue, 0, R.drawable.toggle_on_new, 0);

                } else {
                    isClickFb = 0;
                    etfacebook.setCompoundDrawablesWithIntrinsicBounds(R.drawable.facebook_blue, 0, R.drawable.toogle_off_new, 0);
                }


                break;
            case R.id.Instagram:

                if (isClickInsta == 0) {


                    FollowOnInstagram(otherINSTAID);


                } else {
                    isClickInsta = 0;

                    etInstagram.setCompoundDrawablesWithIntrinsicBounds(R.drawable.instagram, 0, R.drawable.toogle_off_new, 0);

                }

                break;
            case R.id.Twitter:


                if (isClickTwitter == 0) {


                    responsecode = 1002;
                    authorizeTwitter();


                } else {
                    isClickTwitter = 0;
                    etTwitter.setCompoundDrawablesWithIntrinsicBounds(R.drawable.twitter, 0, R.drawable.toogle_off_new, 0);

                }


                break;
            case R.id.Snapchat:

                break;

        }
    }


    public void sendFriendRequest(String facebook_id) {

        //Log.v("AccessToken", AccessToken.getCurrentAccessToken() + "");

        Log.v("AccessTokenuserid", AccessToken.getCurrentAccessToken() + "");

        if (AccessToken.getCurrentAccessToken().getPermissions() != null) {

            if (Common.getPreferences(_activity, "FBAccessToken") != null &&
                    AccessToken.getCurrentAccessToken().getPermissions().contains("publish_actions")) {

                // String url = "http://samples.ogp.me/" + facebook_id;

                //  Log.v("fbId", url);


                Bundle params = new Bundle();
                params.putString("profile", facebook_id);

                //  params.putString("profile", "http://samples.ogp.me/390580850990722");
                //  params.putString("access_token", getClientToken());
/* make the API call */
                new GraphRequest(
                        AccessToken.getCurrentAccessToken(),
                        "/me/og.follows",
                        params,
                        HttpMethod.POST,
                        new GraphRequest.Callback() {

                            public void onCompleted(GraphResponse response) {

                                Log.v("fbResponse", response + "");
                                Log.v("fbResponseError", response.getError() + "");
                                //  JSONObject json = response.getJSONObject();

                                if (response.getError() != null) {
                                    Common.showToast(_activity, "Already follow on facebook!");

                                } else {
                                    SendConnectionToServer("fb", "1");
                                    Common.showToast(_activity, "Successfully on facebook!");
                                }


                            }
                        }
                ).executeAsync();

            } else {
                //login to Facebook

                Common.showToast(_activity, "Access Token Required!");
                LoginManager.getInstance().logInWithPublishPermissions(_activity, Arrays.asList("publish_actions"));
            }

        }


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent kdata) {
        super.onActivityResult(requestCode, resultCode, kdata);


        if (resultCode == Activity.RESULT_OK) {
            if (responsecode == 1001) {
                //FACEBOOK
                callbackManager.onActivityResult(requestCode, resultCode, kdata);

                sendFriendRequest(otherFBID);

            } else if (responsecode == 1002) {
                //TWITTER

                authClient.onActivityResult(requestCode, resultCode, kdata);
            }
        }
        // authClient.onActivityResult(requestCode, resultCode, kdata);


        //   callbackManager.onActivityResult(requestCode, resultCode, kdata);

        // sendFriendRequest(otherUserdata.getFacebook_id());
    }


    public void authorizeTwitter() {

        if (Common.getPreferences(_activity, "Twitter_token").equals("0")) {
            authClient = new TwitterAuthClient();

            authClient.authorize(_activity, new Callback<TwitterSession>() {
                @Override
                public void success(Result<TwitterSession> result) {
                    final TwitterSession session = result.data;
                    if (session != null) {
                        CurrentTwitterSeesion = session;
                        _activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getUserData(session);

                            }
                        });
                    }
                }

                @Override
                public void failure(TwitterException exception) {
                    authClient = null;

                    // Handle exception

                    Common.showToast(_activity, "Twitter authentication Cancelled!");
                }
            });

        } else {
            //follow to twitter...

            ConfigurationBuilder cb = new ConfigurationBuilder();
            cb.setDebugEnabled(true)
                    .setOAuthConsumerKey(TWITTER_KEY)
                    .setOAuthConsumerSecret(TWITTER_SECRET)
                    .setOAuthAccessToken(Common.getPreferences(_activity, "Twitter_token"))
                    .setOAuthAccessTokenSecret(Common.getPreferences(_activity, "Twitter_secret"));
            TwitterFactory tf = new TwitterFactory(cb.build());
            twitter4j.Twitter twitter = tf.getInstance();

            try {


                twitter.createFriendship(otheTwitterID);
                Common.showToast(_activity, "Your twitter follow request sent !");
                isClickTwitter = 0;

                etTwitter.setCompoundDrawablesWithIntrinsicBounds(R.drawable.twitter, 0, R.drawable.toggle_on_new, 0);

                SendConnectionToServer("twitter", "1");
                //  twitter.createFriendship("Shyam030");

            } catch (twitter4j.TwitterException e) {
                e.printStackTrace();
                Common.showToast(_activity, "Something missing." + e.getErrorMessage() + " Please try again");
                isClickTwitter = 0;

                etTwitter.setCompoundDrawablesWithIntrinsicBounds(R.drawable.twitter, 0, R.drawable.toogle_off_new, 0);
            }

        }


    }


    private void getUserData(final TwitterSession session) {
        com.twitter.sdk.android.Twitter.getApiClient(session).getAccountService()
                .verifyCredentials(true, false, new Callback<User>() {

                    @Override
                    public void failure(TwitterException e) {


                    }

                    @Override
                    public void success(Result<User> userResult) {

                        User user = userResult.data;
                        TwitterAuthToken authToken = session.getAuthToken();
                        String token = authToken.token;
                        String secret = authToken.secret;

                        Common.SetPreferences(_activity, "Twitter_token", token);
                        Common.SetPreferences(_activity, "Twitter_secret", secret);

                        try {

                            String TwitterId = String.valueOf(user.id);
                            String username = String.valueOf(user.screenName);


                            System.out.println("user_name---" + String.valueOf(user.id));
                            System.out.println("user_name---" + username);


                            ConfigurationBuilder cb = new ConfigurationBuilder();
                            cb.setDebugEnabled(true)
                                    .setOAuthConsumerKey(TWITTER_KEY)
                                    .setOAuthConsumerSecret(TWITTER_SECRET)
                                    .setOAuthAccessToken(token)
                                    .setOAuthAccessTokenSecret(secret);
                            TwitterFactory tf = new TwitterFactory(cb.build());
                            twitter4j.Twitter twitter = tf.getInstance();

                            try {


                                twitter.createFriendship(otheTwitterID);
                                Common.showToast(_activity, "Your twitter follow request sent !");

                                isClickTwitter = 1;

                                etTwitter.setCompoundDrawablesWithIntrinsicBounds(R.drawable.twitter, 0, R.drawable.toggle_on_new, 0);

                                SendConnectionToServer("twitter", "1");
                                //  twitter.createFriendship("Shyam030");

                            } catch (twitter4j.TwitterException e) {

                                e.printStackTrace();
                                Common.showToast(_activity, "Something missing." + e.getErrorMessage() + " Please try again");
                                isClickTwitter = 0;

                                etTwitter.setCompoundDrawablesWithIntrinsicBounds(R.drawable.twitter, 0, R.drawable.toogle_off_new, 0);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                });

    }


    public void disableBtn(Boolean followSuces) {

        if (followSuces) {
            etInstagram.setEnabled(false);

            isClickInsta = 1;


            etInstagram.setCompoundDrawablesWithIntrinsicBounds(R.drawable.instagram, 0, R.drawable.toggle_on_new, 0);

            SendConnectionToServer("insta", "1");


        } else {
            etInstagram.setEnabled(true);
            isClickInsta = 0;


            etInstagram.setCompoundDrawablesWithIntrinsicBounds(R.drawable.instagram, 0, R.drawable.toggle_off, 0);

        }

    }

    private void SendConnectionToServer(String type, String status) {


        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-connection.php");
        baseRequestData.setTag(ResponseType.ADD_CONTACT);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", mydata.getUserid());
        //  requestedServiceDataModel.putQurry("otherid", "140");
        requestedServiceDataModel.putQurry("otherid", otherUserdata.getUserid());

        switch (type) {
            case "insta":

                requestedServiceDataModel.putQurry("insta", status);

                requestedServiceDataModel.putQurry("twitter", "1");
                requestedServiceDataModel.putQurry("fb", "1");
                requestedServiceDataModel.putQurry("snapchat", otherUserdata.getSnapchat());


             /*   requestedServiceDataModel.putQurry("twitter", otherUserdata.getTwitter());
                requestedServiceDataModel.putQurry("fb", otherUserdata.getFacebook());
                requestedServiceDataModel.putQurry("snapchat", otherUserdata.getSnapchat());*/
                break;

            case "fb":

                requestedServiceDataModel.putQurry("fb", status);
                requestedServiceDataModel.putQurry("twitter", otherUserdata.getTwitter());
                requestedServiceDataModel.putQurry("snapchat", otherUserdata.getSnapchat());
                requestedServiceDataModel.putQurry("insta", otherUserdata.getInstagram());
                break;

            case "twitter":

                requestedServiceDataModel.putQurry("twitter", status);
                requestedServiceDataModel.putQurry("snapchat", otherUserdata.getSnapchat());
                requestedServiceDataModel.putQurry("fb", otherUserdata.getFacebook());
                requestedServiceDataModel.putQurry("insta", otherUserdata.getInstagram());
                break;
            case "snapchat":

                requestedServiceDataModel.putQurry("snapchat", status);
                requestedServiceDataModel.putQurry("twitter", otherUserdata.getTwitter());
                requestedServiceDataModel.putQurry("fb", otherUserdata.getFacebook());
                requestedServiceDataModel.putQurry("insta", otherUserdata.getInstagram());
                break;


        }


        requestedServiceDataModel.setWebServiceType("add");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }
}

