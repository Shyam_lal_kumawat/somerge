package Fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoggingBehavior;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.somerge.ChangePasswordActivity;
import com.somerge.LoginActivity;
import com.somerge.R;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gettersetter.Data;
import instagram_sdk.ApplicationData;
import instagram_sdk.InstagramApp;
import io.fabric.sdk.android.Fabric;
import request.BaseRequestData;
import request.Constant;
import request.RequestedServiceDataModel;
import request.ResponseDelegate;
import request.ResponseType;
import utils.Common;
import utils.CommonMethods;
import utils.LightEditText;
import utils.MediumTextView;
import utils.RegularTextView;
import utils.SemiBoldTextView;

/**
 * Created by user7 on 19/10/16.
 */
public class EditProfileFragment extends Fragment implements ResponseDelegate, GoogleApiClient.OnConnectionFailedListener {


    @Bind(R.id.et_fname)
    LightEditText etFname;
    @Bind(R.id.et_lname)
    LightEditText etLname;
    @Bind(R.id.et_phone)
    LightEditText etPhone;
    @Bind(R.id.et_email)
    LightEditText etEmail;
    @Bind(R.id.toogle)
    SemiBoldTextView toogle;
    @Bind(R.id.facebook)
    MediumTextView facebook;
    @Bind(R.id.bt_fb)
    MediumTextView btFb;
    @Bind(R.id.instagram)
    MediumTextView instagram;
    @Bind(R.id.bt_instagram)
    SemiBoldTextView btInstagram;
    @Bind(R.id.twitter)
    MediumTextView twitter;
    @Bind(R.id.bt_twitter)
    SemiBoldTextView btTwitter;
    @Bind(R.id.snapchat)
    MediumTextView snapchat;
    @Bind(R.id.bt_snapchat)
    SemiBoldTextView btSnapchat;
    @Bind(R.id.next)
    RegularTextView next;
    @Bind(R.id.reset)
    RegularTextView reset;
    @Bind(R.id.ll_bottom)
    LinearLayout llBottom;
    @Bind(R.id.chamgepassowrd)
    RegularTextView chamgepassowrd;
    @Bind(R.id.logout)
    RegularTextView logout;
    private View parentView;
    CommonMethods commanMethods;
    private Activity _activity;
    RecyclerView recyclerView;
    private boolean is_on;

    private boolean isFB, isInsta, isTwitter, isSnap;
    private Data data;
    private RequestedServiceDataModel requestedServiceDataModel;
    private CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;

    private String fbSocialId, social_type, status, userid;

    //Isntagram
    private InstagramApp mApp;
    private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();
    private String InstagramID;
    private int responsecode;

    private TwitterAuthClient authClient;
    private static final String TWITTER_KEY = "hqK7xdX9oEqyKI7fpoORQfx5n"; // "vQhEDS9JOYysgQFhU3XggA3NY";
    private static final String TWITTER_SECRET = "6OgpGYiD2T9NK4uhXmshirwcxgeDbw48i6nTVp0tPdg1XKxTcN";//"veNWQKmGUlLWYbw9nU1dJhKQqwqCa8HYWmiSFdZ5KmlsBfrspm";
    private String TwitterId;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        _activity = getActivity();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(getActivity(), new Twitter(authConfig));
        FacebookSdk.sdkInitialize(_activity);
        AppEventsLogger.activateApp(_activity);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS);
        parentView = inflater.inflate(R.layout.editdetail, null, false);


        commanMethods = new CommonMethods(_activity, this);

        ButterKnife.bind(this, parentView);
        setBodyUI();


        return parentView;


    }

    private void setBodyUI() {
        facebookSDKInitialize();
        googleSigin();

        LoadInstagram();

        Gson gson = new Gson();
        data = gson.fromJson(Common.getPreferences(_activity, "data"), Data.class);
        userid = data.getUserid();
        setProfileData(data);


    }


    private void googleSigin() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(_activity)
                .enableAutoManage((FragmentActivity) _activity, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    private void setProfileData(Data data) {
        etEmail.setEnabled(false);

        etPhone.setText(data.getMobile());
        etFname.setText(data.getFirst_name());
        etLname.setText(data.getLast_name());
        etEmail.setText(data.getEmail());

        if (data.getIs_private().equals("0")) {
            is_on = false;
            toogle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.toogle_off_new, 0);
        } else {
            is_on = true;
            toogle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.toggle_on_new, 0);
        }

        // Set connection status..
        if (data.getSocial() != null) {
            for (int i = 0; i < data.getSocial().size(); i++) {


                setSocialConnection(data.getSocial().get(i).getSocial_site(), data.getSocial().get(i).getSocial_id());
            }

        }


    }

    private void setSocialConnection(String social_site, String social_id) {
        switch (social_site.toUpperCase()) {
            case "FACEBOOK":

                isFB = true;
                btFb.setText("Connected");
                btFb.setTextColor(getResources().getColor(R.color.color_toolbar));
                fbSocialId = social_id;

                break;

            case "INSTAGRAM":

                isInsta = true;
                btInstagram.setText("Connected");
                btInstagram.setTextColor(getResources().getColor(R.color.color_toolbar));
                InstagramID = social_id;


                break;
            case "TWITTER":

                isTwitter = true;
                btTwitter.setText("Connected");
                btTwitter.setTextColor(getResources().getColor(R.color.color_toolbar));
                TwitterId = social_id;


                break;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mGoogleApiClient.stopAutoManage(getActivity());
        mGoogleApiClient.disconnect();
    }


    private void facebookSDKInitialize() {
        FacebookSdk.sdkInitialize(_activity);
        callbackManager = CallbackManager.Factory.create();

        Log.v("AccessTokenEditBefore", AccessToken.getCurrentAccessToken() + "");
        Log.v("AccessTokenEditBefore", AccessToken.getCurrentAccessToken() + "");
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (responsecode == 1001) {
                //FACEBOOK
                callbackManager.onActivityResult(requestCode, resultCode, data);

                RequestData();

            } else if (responsecode == 1002) {
                //TWITTER

                authClient.onActivityResult(requestCode, resultCode, data);
            }
        }

    }


    private void RequestData() {

//        Log.v("AccessTokenEdit", AccessToken.getCurrentAccessToken().getApplicationId() + "");
        // Log.v("AccessTokenEdit", AccessToken.getCurrentAccessToken().getUserId() + "");

        Common.SetPreferences(_activity, "FBAccessToken", AccessToken.getCurrentAccessToken().toString());

        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {


                        Log.v("response", response.toString());


                        JSONObject json = response.getJSONObject();

                        try {
                            if (json != null) {

                                String fbname = json.getString("name");
                                fbSocialId = json.getString("id");


                                isFB = true;
                                btFb.setText("Connected");

                                btFb.setTextColor(getResources().getColor(R.color.color_toolbar));

                                serverRequestAddFacebook(fbSocialId, userid, "FACEBOOK", "1");

                            }

                        } catch (JSONException e) {
                            Common.showToast(_activity, e.toString());
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture.width(150).height(150)");
        request.setParameters(parameters);
        request.executeAsync();

        Log.v("request data is ", request + "");
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        commanMethods.setUpToolbars((AppCompatActivity) _activity, "edit");
    }

    @OnClick({R.id.toogle, R.id.bt_fb, R.id.bt_instagram, R.id.bt_twitter, R.id.bt_snapchat, R.id.next, R.id.reset, R.id.logout, R.id.chamgepassowrd})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toogle:
                if (is_on == false) {
                    is_on = true;
                    commanMethods.Public_Alert(_activity);
                    toogle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.toggle_on_new, 0);

                } else {
                    is_on = false;
                    toogle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.toogle_off_new, 0);
                }
                break;
            case R.id.bt_fb:

                responsecode = 1001;

                if (isFB == false) {

                    LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));


                } else {

                    LoginManager.getInstance().logOut();

                    serverRequestAddFacebook(fbSocialId, userid, "FACEBOOK", "0");

                    isFB = false;
                    btFb.setText("Not Connected");

                    btFb.setTextColor(getResources().getColor(R.color.colorgrey));

                }


                break;
            case R.id.bt_instagram:


                connectOrDisconnectUser();

                break;
            case R.id.bt_twitter:

                responsecode = 1002;

                if (isTwitter == false) {


                    CookieSyncManager.createInstance(_activity);
                    CookieManager cookieManager = CookieManager.getInstance();
                    cookieManager.removeSessionCookie();
                    Twitter.getSessionManager().clearActiveSession();
                    Twitter.logOut();


                    authorizeTwitter();


                } else {


                    Common.SetPreferences(_activity, "Twitter_token", "0");
                    Common.SetPreferences(_activity, "Twitter_secret", "0");



                    serverRequestAddFacebook(TwitterId, userid, "TWITTER", "0");

                    CookieSyncManager.createInstance(_activity);
                    CookieManager cookieManager = CookieManager.getInstance();
                    cookieManager.removeSessionCookie();
                    Twitter.getSessionManager().clearActiveSession();
                    Twitter.logOut();


                    isTwitter = false;
                    btTwitter.setText("Not Connected");
                    btTwitter.setTextColor(getResources().getColor(R.color.colorgrey));
                }
                break;
            case R.id.bt_snapchat:

                if (isSnap == false) {
                    isSnap = true;

                    btSnapchat.setTextColor(getResources().getColor(R.color.color_toolbar));
                    btSnapchat.setText("Connected");


                } else {
                    isSnap = false;
                    btSnapchat.setText("Not Connected");
                    btSnapchat.setTextColor(getResources().getColor(R.color.colorgrey));
                }
                break;
            case R.id.next:

                checkValidation();


                break;
            case R.id.reset:
                etFname.setText("");
                etLname.setText("");
                etPhone.setText("");

                break;

            case R.id.logout:


                AlertDialog myAlertDialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(_activity);
                builder.setTitle("Alert");
                builder.setMessage("Are you sure you want to logout?");
                builder.setPositiveButton("Yes ", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        dialog.dismiss();


                        serverRequestForLogout();

                    }
                });

                builder.setNegativeButton("No ", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        dialog.dismiss();


                    }
                });

                builder.setCancelable(false);
                myAlertDialog = builder.create();
                myAlertDialog.show();

                break;

            case R.id.chamgepassowrd:
                Intent intent = new Intent(_activity, ChangePasswordActivity.class);
                _activity.startActivity(intent);


                break;
        }
    }

    private void LoadInstagram() {
// Here We load instagram in the web view

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mApp = new InstagramApp(_activity, ApplicationData.CLIENT_ID,
                ApplicationData.CLIENT_SECRET, ApplicationData.CALLBACK_URL);
        mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {

            @Override
            public void onSuccess() {

                isInsta = true;

                btInstagram.setTextColor(getResources().getColor(R.color.color_toolbar));
                btInstagram.setText("Connected");

                //  mApp.fetchUserName(handler);


                InstagramID = mApp.getInstaId();
                System.out.println("IsntaID" + InstagramID);


                serverRequestAddFacebook(InstagramID, userid, "INSTAGRAM", "1");


            }

            @Override
            public void onFail(String error) {
                Toast.makeText(_activity, error, Toast.LENGTH_SHORT)
                        .show();
            }

        });


        if (mApp.hasAccessToken()) {
            // tvSummary.setText("Connected as " + mApp.getUserName());
            btInstagram.setText("Connected");
            btInstagram.setTextColor(getResources().getColor(R.color.color_toolbar));
            //  mApp.fetchUserName(handler);


        }


    }

    private Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == InstagramApp.WHAT_FINALIZE) {
                userInfoHashmap = mApp.getUserInfo();
            } else if (msg.what == InstagramApp.WHAT_FINALIZE) {
                Toast.makeText(_activity, "Check your network.",
                        Toast.LENGTH_SHORT).show();
            }
            return false;
        }
    });


    private void connectOrDisconnectUser() {
        if (mApp.hasAccessToken()) {

            serverRequestAddFacebook(InstagramID, userid, "INSTAGRAM", "0");

            mApp.resetAccessToken();
            btInstagram.setText("Not Connected");
            btInstagram.setTextColor(getResources().getColor(R.color.colorgrey));

        } else {
            mApp.authorize();


        }
    }


    private void serverRequestAddFacebook(String social_id, String userid, String social_type, String status) {


        requestedServiceDataModel = new RequestedServiceDataModel(_activity, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.ADD_SOCIAL);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("social_site", social_type);
        requestedServiceDataModel.putQurry("social_id", social_id);
        requestedServiceDataModel.putQurry("status", status);
        requestedServiceDataModel.setWebServiceType("ADD-SOICAL");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();


    }


    private void serverRequestForLogout() {


        requestedServiceDataModel = new RequestedServiceDataModel(_activity, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.LOGOUT);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", data.getUserid());
        requestedServiceDataModel.putQurry("device_token", "");
        requestedServiceDataModel.putQurry("device_type", "ANDROID");
        requestedServiceDataModel.setWebServiceType("LOGOUT");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    private void checkValidation() {

        if (etFname.getText().toString().trim().equalsIgnoreCase("")) {
            Common.showToast(_activity, "Please enter first name.");
        } else if (etLname.getText().toString().trim().equalsIgnoreCase("")) {
            Common.showToast(_activity, "Please enter last name.");
        } else if (etPhone.getText().toString().trim().equalsIgnoreCase("")) {
            Common.showToast(_activity, "Please enter mobile number.");
        } else if (etPhone.getText().toString().trim().length() < 10) {
            Common.showToast(_activity, "Please enter valid phone number");
        } else if (etEmail.getText().toString().trim().equalsIgnoreCase("")) {
            Common.showToast(_activity, "Please enter email address.");
        } else if (!Common.isValidEmail(etEmail.getText().toString().trim())) {
            Common.showToast(_activity, "Please enter valid email address.");
        } else if (Common.getConnectivityStatus(_activity)) {
            serverRequestForEditProfile();
        } else {
            Common.showToast(_activity, "Please check your internet connection.");
        }
    }

    private void serverRequestForEditProfile() {
        requestedServiceDataModel = new RequestedServiceDataModel(_activity, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.EDITPROFILE);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);


        if (is_on == false) {

            requestedServiceDataModel.putQurry("is_private", "0");
        } else {

            requestedServiceDataModel.putQurry("is_private", "1");
        }

        requestedServiceDataModel.putQurry("email", etEmail.getText().toString().trim());
        requestedServiceDataModel.putQurry("userid", data.getUserid());
        requestedServiceDataModel.putQurry("first_name", etFname.getText().toString().trim());
        requestedServiceDataModel.putQurry("last_name", etLname.getText().toString().trim());
        requestedServiceDataModel.putQurry("mobile", etPhone.getText().toString().trim());
        requestedServiceDataModel.setWebServiceType("EDITPROFILE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String jsondata, String message, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.EDITPROFILE:
                Common.showToast(_activity, message);
                Common.SetPreferences(_activity, "data", jsondata);
                _activity.onBackPressed();
//                NextActivity(CreateQRCodeActivity.class);
                break;

            case ResponseType.LOGOUT:
             /*   Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                            }
                        });
*/
                Common.showToast(_activity, message);
                _activity.getSharedPreferences("prefs_login", Activity.MODE_PRIVATE).edit().clear().commit();

                mApp.resetAccessToken();

                LoginManager.getInstance().logOut();


                CookieSyncManager.createInstance(_activity);
                CookieManager cookieManager = CookieManager.getInstance();
                cookieManager.removeSessionCookie();
                Twitter.getSessionManager().clearActiveSession();

                Twitter.logOut();

                Intent intent = new Intent(_activity, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

                break;

            case ResponseType.ADD_SOCIAL:
                utils.Log.log(jsondata);

                Common.showToast(_activity, message);


                break;
        }
    }

    @Override
    public void onFailure(String jsondata, String message, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.EDITPROFILE:
                Common.showToast(_activity, message);
//                NextActivity(CreateQRCodeActivity.class);
                break;
            case ResponseType.LOGOUT:
                Common.showToast(_activity, message);

                break;
            case ResponseType.ADD_SOCIAL:
                Common.showToast(_activity, message);


                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    public void authorizeTwitter() {
        //TwitterAuthClient authClient = new TwitterAuthClient();


        authClient = new TwitterAuthClient();

        authClient.authorize(_activity, new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                final TwitterSession session = result.data;
                if (session != null) {
                    _activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            getUserData(session);


                        }
                    });
                }
            }

            @Override
            public void failure(TwitterException exception) {

                authClient = null;

                Common.showToast(_activity, "Twitter authentication Cancelled!");

                // Handle exception

            }
        });
    }

    private void getUserData(final TwitterSession session) {
        Twitter.getApiClient(session).getAccountService()
                .verifyCredentials(true, false, new Callback<User>() {

                    @Override
                    public void failure(TwitterException e) {

                    }

                    @Override
                    public void success(Result<User> userResult) {

                        User user = userResult.data;
                        //     String twitterImage = user.profileImageUrl;

                        try {

                            TwitterId = String.valueOf(user.id);
                            String twiiterusername = String.valueOf(user.screenName);
                            System.out.println("user_name---" + TwitterId);
                            System.out.println("user_name---" + twiiterusername);

                            isTwitter = true;
                            btTwitter.setTextColor(getResources().getColor(R.color.color_toolbar));
                            btTwitter.setText("Connected");

                            TwitterAuthToken authToken = session.getAuthToken();
                            String token = authToken.token;
                            String secret = authToken.secret;

                            Common.SetPreferences(_activity, "Twitter_token", token);
                            Common.SetPreferences(_activity, "Twitter_secret", secret);

                            serverRequestAddFacebook(twiiterusername, userid, "TWITTER", "1");


                        /*    Log.d("imageurl", user.profileImageUrl);
                            Log.d("name", user.name);
                            Log.d("des", user.description);
                            Log.d("followers ", String.valueOf(user.followersCount));
                            Log.d("createdAt", user.createdAt);*/
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                });

    }

}
