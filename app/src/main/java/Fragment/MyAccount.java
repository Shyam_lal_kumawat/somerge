package Fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.somerge.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gettersetter.Data;
import request.BaseRequestData;
import request.Constant;
import request.RequestedServiceDataModel;
import request.ResponseDelegate;
import request.ResponseType;
import utils.Common;
import utils.CommonMethods;
import utils.LightEditText;
import utils.LightTextView;
import utils.MediumTextView;
import utils.SemiBoldTextView;
import  com.squareup.picasso.Callback;

/**
 * Created by user7 on 19/10/16.
 */
public class MyAccount extends Fragment implements ResponseDelegate {


    @Bind(R.id.img_profile)
    ImageView imgProfile;
    @Bind(R.id.img_qrcode)
    ImageView imgQrcode;
    @Bind(R.id.General)
    SemiBoldTextView General;
    @Bind(R.id.Name)
    LightTextView Name;
    @Bind(R.id.etName)
    LightEditText etName;
    @Bind(R.id.View)
    android.view.View View;
    @Bind(R.id.User_name)
    LightTextView UserName;
    @Bind(R.id.et_username)
    LightEditText etUsername;
    @Bind(R.id.Phone)
    LightTextView Phone;
    @Bind(R.id.et_phone)
    LightEditText etPhone;
    @Bind(R.id.Email)
    LightTextView Email;
    @Bind(R.id.et_email)
    LightEditText etEmail;
    @Bind(R.id.Account)
    TextView Account;
    @Bind(R.id.Set_Profile)
    MediumTextView SetProfile;
    @Bind(R.id.facebook)
    MediumTextView facebook;
    @Bind(R.id.Instagram)
    MediumTextView Instagram;
    @Bind(R.id.Twitter)
    MediumTextView Twitter;
    @Bind(R.id.Snapchat)
    MediumTextView Snapchat;
    @Bind(R.id.rl_facebook)
    RelativeLayout rlFacebook;
    @Bind(R.id.rl_instagram)
    RelativeLayout rlInstagram;
    @Bind(R.id.rl_twitter)
    RelativeLayout rlTwitter;
    @Bind(R.id.rl_snapchat)
    RelativeLayout rlSnapchat;
    @Bind(R.id.progress_bar)
    ProgressBar progressBar;
    private View parentView;
    CommonMethods commanMethods;
    private Activity _activity;

    private boolean isFB, isInsta, isTwitter, isSnap, is_Profile;
    private Data data;
    private RequestedServiceDataModel requestedServiceDataModel;
    private String userid;
    private String clickType;
    private boolean toggleOn;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        _activity = getActivity();
        parentView = inflater.inflate(R.layout.my_account, null, false);


        commanMethods = new CommonMethods(_activity, this);


        ButterKnife.bind(this, parentView);
        setBodyUI();

        return parentView;


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        commanMethods.setUpToolbars((AppCompatActivity) _activity, "My Account");

    }


    private void setBodyUI() {


        Gson gson = new Gson();
        data = gson.fromJson(Common.getPreferences(_activity, "data"), Data.class);
        userid = data.getUserid();

        Log.v("my imagess", Constant.imageBaseUrl + data.getGenerated_image());
        // Glide.with(_activity).load(Constant.imageBaseUrl + data.getGenerated_image()).placeholder(R.drawable.profile).into(imgProfile);

        if (Common.getConnectivityStatus(_activity)) {
            serverRequestForGetProfile(userid);

        } else {
            Common.showToast(_activity, "Please check your internet connection.");

        }



    }

    private void serverRequestForGetProfile(String userid) {
        requestedServiceDataModel = new RequestedServiceDataModel(_activity, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.GETPROFILE);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("otheruserid", userid);
        requestedServiceDataModel.setWebServiceType("GETPROFILE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }


    private void serverRequestSocialConnection(String social_type, String soicalstatus) {
        requestedServiceDataModel = new RequestedServiceDataModel(_activity, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.SOICAL_SETTING);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("social_site", social_type);
        requestedServiceDataModel.putQurry("status", soicalstatus);
        requestedServiceDataModel.setWebServiceType("SOICAL-SETTING");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

        progressBar.setVisibility(View.GONE);
        Common.showToast(_activity, "Failed to load image! Please try again");


    }

    @Override
    public void onSuccess(String jsondata, String message, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GETPROFILE:

                Common.SetPreferences(_activity, "data", jsondata);
                Gson gson = new Gson();

                Data data = gson.fromJson(jsondata, Data.class);

                setProfileData(data);

                break;

            case ResponseType.SOICAL_SETTING:
                utils.Log.log(jsondata);

                Common.showToast(_activity, message);

                if (toggleOn) {
                    setToggleButton(clickType, true);

                } else {
                    setToggleButton(clickType, false);
                }


                break;
        }
    }

    private void setProfileData(Data data) {


        if (etEmail != null && etName != null && etPhone != null && etUsername != null) {
            etEmail.setEnabled(false);
            etName.setEnabled(false);
            etPhone.setEnabled(false);
            etUsername.setEnabled(false);

            etPhone.setText(data.getMobile());
            etName.setText(data.getFirst_name() + " " + data.getLast_name());
            etEmail.setText(data.getEmail());
            etUsername.setText(data.getUsername());

        }


        Log.v("imagess", Constant.imageBaseUrl + data.getGenerated_image());




        if(imgProfile!=null)
        {
            Picasso.with(_activity).load(Constant.imageBaseUrl + data.getGenerated_image())
                    //.memoryPolicy(MemoryPolicy.NO_CACHE)
                    // .networkPolicy(NetworkPolicy.NO_CACHE)
                    .placeholder(R.drawable.profile).into(imgProfile, new Callback() {
                @Override
                public void onSuccess() {

                    if (progressBar != null) {
                        progressBar.setVisibility(View.GONE);
                    }

                }

                @Override
                public void onError() {

                    if (progressBar != null) {
                        progressBar.setVisibility(View.GONE);
                    }

                    Common.showToast(_activity, "Failed to load image! Please try again");

                }
            });
        }

        Picasso.with(_activity).load(Constant.imageBaseUrl + data.getQr_image()).memoryPolicy(MemoryPolicy.NO_CACHE)
                .resize(100, 100).networkPolicy(NetworkPolicy.NO_CACHE).
                placeholder(R.drawable.user_circle).
                        into(imgQrcode);


        if (data.getSocial() != null) {
            for (int i = 0; i < data.getSocial().size(); i++) {
                setSocialConnection(data.getSocial().get(i).getSocial_site(), data.getSocial().get(i).getSocial_id(), data.getSocial().get(i).getStatus());
            }

        }


      /*  if (data.getIs_private().equals("0")) {
            is_Profile = false;
            SetProfile.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.toogle_off_new, 0);
        } else {
            is_Profile = true;
            SetProfile.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.toggle_on_new, 0);
        }
*/

    }



    private void setSocialConnection(String social_site, String social_id, String socialStatus) {
        switch (social_site.toUpperCase()) {
            case "FACEBOOK":

                //check that i have enable FACEBOOK privacy to on so that user can request me .......
                if (socialStatus.equals("1")) {
                    isFB = true;
                    facebook.setCompoundDrawablesWithIntrinsicBounds(R.drawable.facebook_blue, 0, R.drawable.toggle_on_new, 0);

                }

                break;

            case "INSTAGRAM":

                //check that i have enable INSTAGRAM privacy to on so that user can request me .......
                if (socialStatus.equals("1")) {
                    isInsta = true;
                    Instagram.setCompoundDrawablesWithIntrinsicBounds(R.drawable.instagram, 0, R.drawable.toggle_on_new, 0);
                }

                break;
            case "TWITTER":
                //check that i have enable TWITTER privacy to on so that user can request me .......
                if (socialStatus.equals("1")) {

                    isTwitter = true;

                    Twitter.setCompoundDrawablesWithIntrinsicBounds(R.drawable.twitter, 0, R.drawable.toggle_on_new, 0);
                }

                break;
        }
    }

    @Override
    public void onFailure(String jsondata, String message, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GETPROFILE:

                Common.showToast(_activity, message);

                break;
            case ResponseType.SOICAL_SETTING:
                utils.Log.log(jsondata);

                Common.showToast(_activity, message);


                setToggleButton(clickType, false);


                break;
        }
    }

    private void setToggleButton(String clickType, Boolean responseType) {


        switch (clickType.toUpperCase()) {
            case "FACEBOOK":
                if (responseType) {
                    //   isFB = true;
                    facebook.setCompoundDrawablesWithIntrinsicBounds(R.drawable.facebook_blue, 0, R.drawable.toggle_on_new, 0);

                } else {
                    //   isFB = false;
                    facebook.setCompoundDrawablesWithIntrinsicBounds(R.drawable.facebook_blue, 0, R.drawable.toogle_off_new, 0);


                }


                break;

            case "INSTAGRAM":

                if (responseType) {        //      isInsta = true;
                    Instagram.setCompoundDrawablesWithIntrinsicBounds(R.drawable.instagram, 0, R.drawable.toggle_on_new, 0);
                } else {
                    Instagram.setCompoundDrawablesWithIntrinsicBounds(R.drawable.instagram, 0, R.drawable.toogle_off_new, 0);

                }


                break;
            case "TWITTER":

                if (responseType) {
                    //  isTwitter = true;

                    Twitter.setCompoundDrawablesWithIntrinsicBounds(R.drawable.twitter, 0, R.drawable.toggle_on_new, 0);
                } else {
                    Twitter.setCompoundDrawablesWithIntrinsicBounds(R.drawable.twitter, 0, R.drawable.toogle_off_new, 0);
                }
                break;
        }


    }

    @OnClick({R.id.img_profile, R.id.img_qrcode, R.id.Set_Profile, R.id.facebook, R.id.Instagram, R.id.Twitter, R.id.Snapchat})
    public void onClick(android.view.View view) {
        switch (view.getId()) {
            case R.id.img_profile:

                if (imgQrcode.getVisibility() == android.view.View.VISIBLE) {
                    imgQrcode.setVisibility(android.view.View.INVISIBLE);
                } else {
                    imgQrcode.setVisibility(android.view.View.VISIBLE);

                }

                break;
            case R.id.img_qrcode:
                break;
            case R.id.Set_Profile:
                if (is_Profile == false) {
                    is_Profile = true;
                    commanMethods.Public_Alert(_activity);
                    SetProfile.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.toggle_on_new, 0);
                } else {
                    is_Profile = false;
                    SetProfile.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.toogle_off_new, 0);
                }
                break;
            case R.id.facebook:
                if (isFB == false) {

                    isFB = true;
                    clickType = "FACEBOOK";
                    toggleOn = true;
                    serverRequestSocialConnection("FACEBOOK", "1");

                    //  facebook.setCompoundDrawablesWithIntrinsicBounds(R.drawable.facebook_blue, 0, R.drawable.toggle_on_new, 0);

                } else {
                    isFB = false;
                    clickType = "FACEBOOK";

                    toggleOn = false;

                    serverRequestSocialConnection("FACEBOOK", "0");


                    //  facebook.setCompoundDrawablesWithIntrinsicBounds(R.drawable.facebook_blue, 0, R.drawable.toogle_off_new, 0);


                }
                break;
            case R.id.Instagram:
                if (isInsta == false) {
                    isInsta = true;

                    clickType = "INSTAGRAM";
                    toggleOn = true;

                    //Instagram.setCompoundDrawablesWithIntrinsicBounds(R.drawable.instagram, 0, R.drawable.toggle_on_new, 0);

                    serverRequestSocialConnection("INSTAGRAM", "1");


                } else {
                    isInsta = false;
                    clickType = "INSTAGRAM";
                    toggleOn = false;
                    //    Instagram.setCompoundDrawablesWithIntrinsicBounds(R.drawable.instagram, 0, R.drawable.toogle_off_new, 0);

                    serverRequestSocialConnection("INSTAGRAM", "0");

                }
                break;
            case R.id.Twitter:
                if (isTwitter == false) {
                    isTwitter = true;
                    clickType = "TWITTER";
                    toggleOn = true;

                    //Twitter.setCompoundDrawablesWithIntrinsicBounds(R.drawable.twitter, 0, R.drawable.toggle_on_new, 0);
                    serverRequestSocialConnection("TWITTER", "1");
                } else {
                    isTwitter = false;

                    clickType = "TWITTER";
                    toggleOn = false;
                    // Twitter.setCompoundDrawablesWithIntrinsicBounds(R.drawable.twitter, 0, R.drawable.toogle_off_new, 0);

                    serverRequestSocialConnection("TWITTER", "0");
                }
                break;
            case R.id.Snapchat:
                if (isSnap == false) {
                    isSnap = true;

                    clickType = "SNAPCHAT";
                    toggleOn = true;
                    //     Snapchat.setCompoundDrawablesWithIntrinsicBounds(R.drawable.snapchat, 0, R.drawable.toggle_on_new, 0);

                    serverRequestSocialConnection("SNAPCHAT", "1");
                } else {
                    isSnap = false;
                    clickType = "SNAPCHAT";
                    toggleOn = false;

                    // Snapchat.setCompoundDrawablesWithIntrinsicBounds(R.drawable.snapchat, 0, R.drawable.toogle_off_new, 0);
                    serverRequestSocialConnection("SNAPCHAT", "0");

                }
                break;
        }
    }
}
