package com.somerge;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gettersetter.Data;
import instagram_sdk.ApplicationData;
import instagram_sdk.InstagramApp;
import io.fabric.sdk.android.Fabric;
import request.BaseRequestData;
import request.Constant;
import request.RequestedServiceDataModel;
import request.ResponseDelegate;
import request.ResponseType;
import utils.Common;
import utils.CommonMethods;
import utils.LightEditText;
import utils.MediumTextView;
import utils.RegularTextView;
import utils.SemiBoldTextView;

/**
 * Created by user7 on 18/10/16.
 */
public class BasicDetailActivity extends AppCompatActivity implements ResponseDelegate {


    @Bind(R.id.img_left)
    ImageView imgLeft;
    @Bind(R.id.middlename)
    SemiBoldTextView middlename;
    @Bind(R.id.img_right)
    ImageView imgRight;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.et_fname)
    LightEditText etFname;
    @Bind(R.id.et_lname)
    LightEditText etLname;
    @Bind(R.id.et_phone)
    LightEditText etPhone;
    @Bind(R.id.et_email)
    LightEditText etEmail;
    @Bind(R.id.toogle)
    SemiBoldTextView toogle;
    @Bind(R.id.toggle_btn)
    ImageView toggleBtn;
    @Bind(R.id.facebook)
    MediumTextView facebook;
    @Bind(R.id.bt_fb)
    MediumTextView btFb;
    @Bind(R.id.instagram)
    MediumTextView instagram;
    @Bind(R.id.bt_instagram)
    SemiBoldTextView btInstagram;
    @Bind(R.id.twitter)
    MediumTextView twitter;
    @Bind(R.id.bt_twitter)
    SemiBoldTextView btTwitter;
    @Bind(R.id.snapchat)
    MediumTextView snapchat;
    @Bind(R.id.bt_snapchat)
    SemiBoldTextView btSnapchat;
    @Bind(R.id.next)
    RegularTextView next;
    @Bind(R.id.reset)
    RegularTextView reset;
    @Bind(R.id.ll_bottom)
    LinearLayout llBottom;
    private Activity activity;
    private boolean is_on;

    private boolean isFB, isInsta, isTwitter, isSnap;
    private CommonMethods commanMethods;
    private Data data;
    private RequestedServiceDataModel requestedServiceDataModel;


    //Isntagram
    private InstagramApp mApp;
    private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();
    private String InstagramID;
    private int responsecode;

    private TwitterAuthClient authClient;

    private static final String TWITTER_KEY = "hqK7xdX9oEqyKI7fpoORQfx5n"; // "vQhEDS9JOYysgQFhU3XggA3NY";
    private static final String TWITTER_SECRET = "6OgpGYiD2T9NK4uhXmshirwcxgeDbw48i6nTVp0tPdg1XKxTcN";//"veNW
    private String TwitterId;
    private CallbackManager callbackManager;
    private String userid;

    private String fbSocialId;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.basicdetail);


        activity = this;

        //Social Connection  variable

        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(activity, new Twitter(authConfig));
        FacebookSdk.sdkInitialize(activity);
        AppEventsLogger.activateApp(activity);

        //Social Connection  variable

        ButterKnife.bind(this);

        setUI();


    }

    private void setUI() {

        commanMethods = new CommonMethods(activity, this);

        commanMethods.setUpToolbars((AppCompatActivity) activity, "Basic Info");


        facebookSDKInitialize();
        LoadInstagram();


        Gson gson = new Gson();
        data = gson.fromJson(Common.getPreferences(activity, "data"), Data.class);
        userid = data.getUserid();


        setDefaultData();

    }

    /* Facebook load here... */
    private void facebookSDKInitialize() {
        FacebookSdk.sdkInitialize(activity);
        callbackManager = CallbackManager.Factory.create();
    }/* Facebook load here... */


    /* Instagram load here... */
    private void LoadInstagram() {

// Here We load instagram in the web view

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mApp = new InstagramApp(activity, ApplicationData.CLIENT_ID,
                ApplicationData.CLIENT_SECRET, ApplicationData.CALLBACK_URL);
        mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {

            @Override
            public void onSuccess() {

                isInsta = true;

                btInstagram.setTextColor(getResources().getColor(R.color.color_toolbar));
                btInstagram.setText("Connected");

                //  mApp.fetchUserName(handler);


                InstagramID = mApp.getInstaId();
                System.out.println("IsntaID" + InstagramID);


                serverRequestAddFacebook(InstagramID, userid, "INSTAGRAM", "1");


            }

            @Override
            public void onFail(String error) {
                Toast.makeText(activity, error, Toast.LENGTH_SHORT)
                        .show();
            }

        });


        if (mApp.hasAccessToken()) {
            // tvSummary.setText("Connected as " + mApp.getUserName());
            btInstagram.setText("Connected");
            btInstagram.setTextColor(getResources().getColor(R.color.color_toolbar));
            //  mApp.fetchUserName(handler);


        }


    }/* Instagram load here... */


    /* Instagram to check if connected or not  here... */
    private void connectOrDisconnectUser() {
        if (mApp.hasAccessToken()) {

            serverRequestAddFacebook(InstagramID, userid, "INSTAGRAM", "0");

            mApp.resetAccessToken();
            btInstagram.setText("Not Connected");
            btInstagram.setTextColor(getResources().getColor(R.color.colorgrey));

        } else {
            mApp.authorize();


        }
    }/* Instagram to check if connected or not  here... */

    /* Twitter load here... */
    public void authorizeTwitter() {
        //TwitterAuthClient authClient = new TwitterAuthClient();

        authClient = new TwitterAuthClient();

        authClient.authorize(activity, new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                final TwitterSession session = result.data;
                if (session != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            getUserData(session);

                        }
                    });
                }
            }

            @Override
            public void failure(TwitterException exception) {
                authClient = null;

                // Handle exception
            }
        });
    }

    private void getUserData(final TwitterSession session) {
        Twitter.getApiClient(session).getAccountService()
                .verifyCredentials(true, false, new Callback<User>() {

                    @Override
                    public void failure(TwitterException e) {

                    }

                    @Override
                    public void success(Result<User> userResult) {

                        User user = userResult.data;
                        //     String twitterImage = user.profileImageUrl;

                        try {

                            TwitterId = String.valueOf(user.id);
                            String username = String.valueOf(user.screenName);
                            System.out.println("user_name---" + TwitterId);
                            System.out.println("user_name---" + username);

                            isTwitter = true;
                            btTwitter.setTextColor(getResources().getColor(R.color.color_toolbar));
                            btTwitter.setText("Connected");

                            TwitterAuthToken authToken = session.getAuthToken();
                            String token = authToken.token;
                            String secret = authToken.secret;

                            Common.SetPreferences(activity, "Twitter_token", token);
                            Common.SetPreferences(activity, "Twitter_secret", secret);


                            serverRequestAddFacebook(TwitterId, userid, "TWITTER", "1");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                });

    }
    /* Twitter load here... */


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (responsecode == 1001) {
                //FACEBOOK
                callbackManager.onActivityResult(requestCode, resultCode, data);

                RequestData();

            } else if (responsecode == 1002) {
                //TWITTER
                authClient.onActivityResult(requestCode, resultCode, data);
            }


        }
    }


    //Facebook Request user data from facebook Server ..
    private void RequestData() {

        Common.SetPreferences(activity, "FBAccessToken", AccessToken.getCurrentAccessToken().toString());

        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {


                        Log.v("response", response.toString());


                        JSONObject json = response.getJSONObject();

                        try {
                            if (json != null) {

                                String fbname = json.getString("name");
                                fbSocialId = json.getString("id");

                                isFB = true;
                                btFb.setText("Connected");

                                btFb.setTextColor(getResources().getColor(R.color.color_toolbar));

                                serverRequestAddFacebook(fbSocialId, userid, "FACEBOOK", "1");

                            }

                        } catch (JSONException e) {
                            Common.showToast(activity, e.toString());
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture.width(150).height(150)");
        request.setParameters(parameters);
        request.executeAsync();

        Log.v("request data is ", request + "");
    }

    private void setDefaultData() {
        Gson gson = new Gson();
        data = gson.fromJson(Common.getPreferences(this, "data"), Data.class);
        if (!data.getEmail().equals("")) {
            etEmail.setEnabled(false);
            etEmail.setText(data.getEmail());
        }
        if (data.getIs_private().equals("0")) {
            is_on = false;
            toggleBtn.setImageDrawable(getResources().getDrawable(R.drawable.toogle_off_new));

        } else {
            is_on = true;
            toggleBtn.setImageDrawable(getResources().getDrawable(R.drawable.toggle_on_new));
        }


        // Set connection status..
        if (data.getSocial() != null) {
            for (int i = 0; i < data.getSocial().size(); i++) {


                setSocialConnection(data.getSocial().get(i).getSocial_site(), data.getSocial().get(i).getSocial_id());
            }

        }

        setFacebookSelected(data.getFacebook());






    }

   public void setFacebookSelected(String facebook)
    {
        isFB = true;
        btFb.setText("Connected");
        btFb.setTextColor(getResources().getColor(R.color.color_toolbar));
        fbSocialId = facebook;


    }


    private void setSocialConnection(String social_site, String social_id) {
        switch (social_site.toUpperCase()) {
            case "FACEBOOK":

                isFB = true;
                btFb.setText("Connected");
                btFb.setTextColor(getResources().getColor(R.color.color_toolbar));
                fbSocialId = social_id;

                break;

            case "INSTAGRAM":

                isInsta = true;
                btInstagram.setText("Connected");
                btInstagram.setTextColor(getResources().getColor(R.color.color_toolbar));
                InstagramID = social_id;


                break;
            case "TWITTER":

                isTwitter = true;
                btTwitter.setText("Connected");
                btTwitter.setTextColor(getResources().getColor(R.color.color_toolbar));
                TwitterId = social_id;


                break;
        }
    }

    @OnClick({R.id.toogle, R.id.toggle_btn, R.id.bt_fb, R.id.bt_instagram, R.id.bt_twitter, R.id.bt_snapchat, R.id.next, R.id.reset})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toogle:
                break;
            case R.id.toggle_btn:

                if (is_on == false) {
                    is_on = true;
                    commanMethods.Public_Alert(activity);

                    toggleBtn.setImageDrawable(getResources().getDrawable(R.drawable.toggle_on_new));


                } else {
                    is_on = false;
                    toggleBtn.setImageDrawable(getResources().getDrawable(R.drawable.toogle_off_new));
                }

                break;
            case R.id.bt_fb:


                responsecode = 1001;

                if (isFB == false) {

                    LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));


                /*    isFB = true;
                    btFb.setText("Connected");

                    btFb.setTextColor(getResources().getColor(R.color.color_toolbar));*/


                } else {

                    Common.SetPreferences(activity, "FBAccessToken", "0");

                    serverRequestAddFacebook(fbSocialId, userid, "FACEBOOK", "0");

                    isFB = false;
                    btFb.setText("Not Connected");

                    btFb.setTextColor(getResources().getColor(R.color.colorgrey));

                }


                break;
            case R.id.bt_instagram:

                connectOrDisconnectUser();
                break;
            case R.id.bt_twitter:

                responsecode = 1002;

                if (isTwitter == false) {


                    Twitter.getSessionManager().clearActiveSession();
                    Twitter.logOut();


                    authorizeTwitter();



                } else {


                    Common.SetPreferences(activity, "Twitter_token", "0");
                    Common.SetPreferences(activity, "Twitter_secret", "0");


                    serverRequestAddFacebook(TwitterId, userid, "TWITTER", "0");


                    isTwitter = false;
                    btTwitter.setText("Not Connected");
                    btTwitter.setTextColor(getResources().getColor(R.color.colorgrey));
                }
                break;
            case R.id.bt_snapchat:

                if (isSnap == false) {
                    isSnap = true;

                    btSnapchat.setTextColor(getResources().getColor(R.color.color_toolbar));
                    btSnapchat.setText("Connected");


                } else {
                    isSnap = false;
                    btSnapchat.setText("Not Connected");
                    btSnapchat.setTextColor(getResources().getColor(R.color.colorgrey));
                }
                break;
            case R.id.next:

                checkValidation();
//                NextActivity(CreateQRCodeActivity.class);

                break;
            case R.id.reset:
                etFname.setText("");
                etLname.setText("");
//                etEmail.setText("");
                etPhone.setText("");
//                etEmail.setEnabled(true);

                break;
        }
    }

    private void checkValidation() {

        if (etFname.getText().toString().trim().equalsIgnoreCase("")) {
            Common.showToast(this, "Please enter first name.");
        } else if (etLname.getText().toString().trim().equalsIgnoreCase("")) {
            Common.showToast(this, "Please enter last name.");
        } else if (etPhone.getText().toString().trim().equalsIgnoreCase("")) {
            Common.showToast(this, "Please enter mobile number.");
        } else if (etPhone.getText().toString().trim().length() < 10) {
            Common.showToast(this, "Please enter valid phone number");
        } else if (etEmail.getText().toString().trim().equalsIgnoreCase("")) {
            Common.showToast(this, "Please enter email address.");
        } else if (!Common.isValidEmail(etEmail.getText().toString().trim())) {
            Common.showToast(this, "Please enter valid email address.");
        } else if (Common.getConnectivityStatus(this)) {
            serverRequestForEditProfile();
        } else {
            Common.showToast(this, "Please check your internet connection.");
        }

    }


    private void serverRequestAddFacebook(String social_id, String userid, String social_type, String status) {


        requestedServiceDataModel = new RequestedServiceDataModel(activity, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.ADD_SOCIAL);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("social_site", social_type);
        requestedServiceDataModel.putQurry("social_id", social_id);
        requestedServiceDataModel.putQurry("status", status);
        requestedServiceDataModel.setWebServiceType("ADD-SOICAL");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();


    }


    private void serverRequestForEditProfile() {
        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.EDITPROFILE);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);


        if (is_on == false) {

            requestedServiceDataModel.putQurry("is_private", "0");
        } else {

            requestedServiceDataModel.putQurry("is_private", "1");
        }

        requestedServiceDataModel.putQurry("email", etEmail.getText().toString().trim());
        requestedServiceDataModel.putQurry("userid", data.getUserid());
        requestedServiceDataModel.putQurry("first_name", etFname.getText().toString().trim());
        requestedServiceDataModel.putQurry("last_name", etLname.getText().toString().trim());
        requestedServiceDataModel.putQurry("mobile", etPhone.getText().toString().trim());
        requestedServiceDataModel.setWebServiceType("EDITPROFILE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }


    public void NextActivity(Class<?> context) {
        Intent intent = new Intent(activity, context);
        startActivity(intent);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        finish();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String jsondata, String message, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.EDITPROFILE:

                Common.SetPreferences(this, "data", jsondata);


                NextActivity(CreateQRCodeActivity.class);
//                NextActivity(CommanFragmentActitvty.class);
                break;

            case ResponseType.ADD_SOCIAL:
                utils.Log.log(jsondata);

                Common.showToast(activity, message);


                break;
        }

    }

    @Override
    public void onFailure(String jsondata, String message, BaseRequestData baseRequestData) {

    }
}
