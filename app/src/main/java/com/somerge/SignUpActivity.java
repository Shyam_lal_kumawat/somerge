package com.somerge;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gettersetter.Data;
import request.BaseRequestData;
import request.Constant;
import request.RequestedServiceDataModel;
import request.ResponseDelegate;
import request.ResponseType;
import utils.Common;
import utils.LightEditText;
import utils.RegularTextView;
import utils.SemiBoldTextView;

/**
 * Created by user7 on 13/10/16.
 */
public class SignUpActivity extends AppCompatActivity implements ResponseDelegate, GoogleApiClient.OnConnectionFailedListener {


    @Bind(R.id.et_username)
    LightEditText etUsername;
    @Bind(R.id.et_email)
    LightEditText etEmail;
    @Bind(R.id.et_password)
    LightEditText etPassword;
    @Bind(R.id.et_confirmpassword)
    LightEditText etConfirmpassword;
    @Bind(R.id.toogle)
    SemiBoldTextView toogle;
    @Bind(R.id.toggle_btn)
    RegularTextView toggleBtn;
    @Bind(R.id.signup)
    RegularTextView signup;
    @Bind(R.id.xIcon)
    ImageView xIcon;
    @Bind(R.id.bt_fb)
    RegularTextView btFb;
    @Bind(R.id.rl_fb)
    LinearLayout rlFb;
    @Bind(R.id.gIcon)
    ImageView gIcon;
    @Bind(R.id.bt_google)
    RegularTextView btGoogle;
    @Bind(R.id.rl_google)
    LinearLayout rlGoogle;
    @Bind(R.id.login)
    RegularTextView login;
    private Activity activity;
    private boolean is_on;

    private Dialog dialog;
    private CallbackManager callbackManager;
    private String name, email;
    private RequestedServiceDataModel requestedServiceDataModel;

    private String social_type = "";
    private String social_id = "";
    private GoogleApiClient mGoogleApiClient;
    private int RC_SIGN_IN = 007;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.signup);
        ButterKnife.bind(this);

        googleSigin();

        setUI();


    }

    private void googleSigin() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void setUI() {

        activity = this;

        facebookSDKInitialize();


        etPassword.setTypeface(Typeface.createFromAsset(activity.getAssets(),
                "latollight.ttf"));
        etPassword.setTransformationMethod(new PasswordTransformationMethod());

        etConfirmpassword.setTypeface(Typeface.createFromAsset(activity.getAssets(),
                "latollight.ttf"));
        etConfirmpassword.setTransformationMethod(new PasswordTransformationMethod());

        getDefaultData();


    }

    private void getDefaultData() {
        String type = getIntent().getStringExtra("type");
        if (type.equals("clickuser")) {

            setData();

        } else {

        }

    }

    private void setData() {
        email = getIntent().getStringExtra("email");

        name = getIntent().getStringExtra("name");

        if (!email.equals("")) {
            etEmail.setText(email);
            etEmail.setEnabled(false);
        }

        if (!etUsername.equals("")) {
            etUsername.setText(name);
        }

        social_type = getIntent().getStringExtra("social_type");
        social_id = getIntent().getStringExtra("social_id");
    }

    private void facebookSDKInitialize() {
        callbackManager = CallbackManager.Factory.create();
    }

    @OnClick({R.id.signup, R.id.rl_fb, R.id.rl_google, R.id.login, R.id.toggle_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signup:
                if (social_type.equals("")) {
                    checkValidation("", "");
                } else if (social_type.equals("facebook")) {
                    checkValidation("FACEBOOK", social_id);
                } else if (social_type.equals("google")) {
                    checkValidation("GOOGLE", social_id);
                }


//                NextActivity(BasicDetailActivity.class);
                break;
            case R.id.rl_fb:

                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
//                NextActivity(BasicDetailActivity.class);

                break;
            case R.id.rl_google:

                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);

//                NextActivity(BasicDetailActivity.class);


                break;
            case R.id.login:
                NextActivity(LoginActivity.class);

                break;
            case R.id.toggle_btn:

                if (is_on == false) {
                    is_on = true;
                    Public_Alert();
                    toggleBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.toggle_on_new, 0);
                } else {
                    is_on = false;
                    toggleBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.toogle_off_new, 0);
                }

                break;
        }

    }

    private void checkValidation(String social_type, String social_id) {

        if (etUsername.getText().toString().trim().equalsIgnoreCase("")) {
            Common.showToast(this, "Please enter user name.");
        } else if (etEmail.getText().toString().trim().equalsIgnoreCase("")) {
            Common.showToast(this, "Please enter email address.");
        } else if (!Common.isValidEmail(etEmail.getText().toString().trim())) {
            Common.showToast(this, "Please enter valid email address.");
        } else if (etPassword.getText().toString().trim().equalsIgnoreCase("")) {
            Common.showToast(this, "Please enter password.");
        } else if (etPassword.length() < 6 || etPassword.length() > 16) {
            Common.showToast(this, "Password length must be between 6 to 16 characters");
        } else if (etConfirmpassword.getText().toString().trim().equalsIgnoreCase("")) {
            Common.showToast(this, "Please enter confirm Password.");
        } else if (!etPassword.getText().toString().trim().equalsIgnoreCase(etConfirmpassword.getText().toString().trim())) {
            Common.showToast(this, "Please enter same confirm Password.");
        } else if (Common.getConnectivityStatus(this)) {
            serverRequestForSingUp(social_type, social_id);
        } else {
            Common.showToast(this, "Please check your internet connection.");
        }

    }

    private void serverRequestForSingUp(String social_type, String social_id) {
        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.SIGNUP);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);


        if (is_on == false) {

            requestedServiceDataModel.putQurry("is_private", "0");
        } else {

            requestedServiceDataModel.putQurry("is_private", "1");
        }

        requestedServiceDataModel.putQurry("email", etEmail.getText().toString().trim());
        requestedServiceDataModel.putQurry("username", etUsername.getText().toString().trim());
        requestedServiceDataModel.putQurry("password", etPassword.getText().toString().trim());
        requestedServiceDataModel.putQurry("device_type", "ANDROID");
        requestedServiceDataModel.putQurry("device_token", "");
        requestedServiceDataModel.putQurry("social_type", social_type);
        requestedServiceDataModel.putQurry("social_id", social_id);
        requestedServiceDataModel.setWebServiceType("SIGNUP");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    public void NextActivity(Class<?> context)

    {
        Intent intent = new Intent(activity, context);
        startActivity(intent);

        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

    }

    private void Public_Alert() {


        dialog = new Dialog(activity, R.style.DialogSlideAnim);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.setContentView(R.layout.alert_public);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        dialog.findViewById(R.id.OK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);

            RequestData();
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e("name", "display name: " + acct.getDisplayName());

            String personName = acct.getDisplayName();
          //  String personPhotoUrl = acct.getPhotoUrl().toString();

            email = acct.getEmail();

            Log.e("detail", "Name: " + personName + ", email: " + email
                    + ", Image: " + " google id" + acct.getId());

            social_type = "google";
            social_id = acct.getId();


            serverRequestForCheckUser(social_id, email, social_type);

//            Glide.with(getApplicationContext()).load(personPhotoUrl)
//                    .thumbnail(0.5f)
//                    .crossFade()
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(imgProfilePic);

        } else {
            // Signed out, show unauthenticated UI.
//            updateUI(false);
        }
    }

    private void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {


                        Log.v("response", response.toString());


                        JSONObject json = response.getJSONObject();

                        try {
                            if (json != null) {

                                name = json.getString("name");
                                social_id = json.getString("id");


                                try {
                                    if (object.has("email")) {
                                        email = object.getString("email");
                                    } else {
                                        email = "";
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                social_type = "facebook";
                                serverRequestForCheckUser(social_id, email, social_type);

                            }

                        } catch (JSONException e) {
                            Common.showToast(SignUpActivity.this, e.toString());
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture.width(150).height(150)");
        request.setParameters(parameters);
        request.executeAsync();

        Log.v("request data is ", request + "");
    }

    private void serverRequestForCheckUser(String social_id, String email, String social_type) {

        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.CHECKUSER);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);

        if (social_type.equals("facebook")) {
            requestedServiceDataModel.putQurry("social_id", social_id);
            requestedServiceDataModel.putQurry("social_type", "FACEBOOK");
        } else if (social_type.equals("google")) {
            requestedServiceDataModel.putQurry("social_id", social_id);
            requestedServiceDataModel.putQurry("social_type", "GOOGLE");
        }


        requestedServiceDataModel.putQurry("email", email);
        requestedServiceDataModel.setWebServiceType("CHECKUSER");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String jsondata, String message, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.SIGNUP:
                Common.showToast(this, message);

                Common.SetPreferences(this, "data", jsondata);

                Intent intent = new Intent(this, BasicDetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();

                break;
            case ResponseType.CHECKUSER:

                if (!email.equals("")) {
                    etEmail.setText(email);
                    etEmail.setEnabled(false);
                }

                break;
        }
    }

    @Override
    public void onFailure(String jsondata, String message, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.CHECKUSER:
                Common.SetPreferences(this, "data", jsondata);
                Gson gson = new Gson();
                Data data = gson.fromJson(jsondata, Data.class);

                if (data.getEmail().equals("") || (data.getFirst_name().equals("")) || (data.getLast_name().equals("")) || (data.getMobile().equals(""))) {
                    Intent intent = new Intent(this, BasicDetailActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {

                    Intent intent = new Intent(this, CommanFragmentActitvty.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }

                break;
            case ResponseType.SIGNUP:
                Common.showToast(this, message);
                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    /* Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        updateUI(false);
                    }
                });*/
}
