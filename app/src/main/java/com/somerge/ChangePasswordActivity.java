package com.somerge;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;

import com.google.gson.Gson;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gettersetter.Data;
import request.BaseRequestData;
import request.Constant;
import request.RequestedServiceDataModel;
import request.ResponseDelegate;
import request.ResponseType;
import utils.Common;
import utils.CommonMethods;
import utils.LightEditText;
import utils.RegularTextView;

/**
 * Created by user7 on 18/10/16.
 */
public class ChangePasswordActivity extends AppCompatActivity implements ResponseDelegate {


    @Bind(R.id.submit)
    RegularTextView submit;
    @Bind(R.id.et_oldPass)
    LightEditText etOldPass;
    @Bind(R.id.et_newPass)
    LightEditText etNewPass;
    @Bind(R.id.et_confirmPass)
    LightEditText etConfirmPass;
    private Activity activity;
    private RequestedServiceDataModel requestedServiceDataModel;
    private Data data;
    private CommonMethods commanMethods;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_pass);
        ButterKnife.bind(this);

        setUI();


    }

    private void setUI() {

        activity = this;
        commanMethods = new CommonMethods(activity, this);

        commanMethods.setUpToolbars((AppCompatActivity) activity, "Change Password");
        Gson gson = new Gson();
        data = gson.fromJson(Common.getPreferences(activity, "data"), Data.class);

        etOldPass.setTypeface(Typeface.createFromAsset(activity.getAssets(),
                "latollight.ttf"));
        etOldPass.setTransformationMethod(new PasswordTransformationMethod());

        etNewPass.setTypeface(Typeface.createFromAsset(activity.getAssets(),
                "latollight.ttf"));
        etNewPass.setTransformationMethod(new PasswordTransformationMethod());

        etConfirmPass.setTypeface(Typeface.createFromAsset(activity.getAssets(),
                "latollight.ttf"));
        etConfirmPass.setTransformationMethod(new PasswordTransformationMethod());


    }

    @OnClick(R.id.submit)
    public void onClick() {
        checkValidation();

//        activity.onBackPressed();
    }

    private void checkValidation() {
        if (etOldPass.getText().toString().trim().equalsIgnoreCase("")) {
            Common.showToast(activity, "Please enter old password.");
        } else if (etOldPass.length() < 6 || etOldPass.length() > 16) {
            Common.showToast(activity, "Password length must be between 6 to 16 characters");
        } else if (etNewPass.getText().toString().trim().equalsIgnoreCase("")) {
            Common.showToast(activity, "Please enter new password.");
        } else if (etNewPass.length() < 6 || etNewPass.length() > 16) {
            Common.showToast(activity, "Password length must be between 6 to 16 characters");
        } else if (etConfirmPass.getText().toString().trim().equalsIgnoreCase("")) {
            Common.showToast(activity, "Please enter confirm Password.");
        } else if (!etNewPass.getText().toString().trim().equalsIgnoreCase(etConfirmPass.getText().toString().trim())) {
            Common.showToast(activity, "Please enter same confirm Password.");
        } else {
            serverRequestForChangePassword();
        }
    }

    private void serverRequestForChangePassword() {
        requestedServiceDataModel = new RequestedServiceDataModel(activity, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.CHANGE);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);

        requestedServiceDataModel.putQurry("userid", data.getUserid());
        requestedServiceDataModel.putQurry("oldpass", etOldPass.getText().toString().trim());
        requestedServiceDataModel.putQurry("pass", etNewPass.getText().toString().trim());
        requestedServiceDataModel.setWebServiceType("CHANGE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String jsondata, String message, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.CHANGE:
                Common.showToast(activity, message);
                onBackPressed();

                break;
        }
    }

    @Override
    public void onFailure(String jsondata, String message, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.CHANGE:
                Common.showToast(activity, message);

                break;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
