package com.somerge;

import android.*;
import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gettersetter.Data;
import photoCrop.CropImage;
import qr.imagecanvas.draw.dao.QRColorData;
import qr.imagecanvas.draw.dao.QRColorUnitData;
import utils.Common;
import utils.CommonMethods;
import utils.MediumTextView;
import utils.RegularTextView;
import utils.SemiBoldTextView;
import utils.Utility;

/**
 * Created by user7 on 18/10/16.
 */
public class CreateQRCodeActivity extends AppCompatActivity {


    @Bind(R.id.img_left)
    ImageView imgLeft;
    @Bind(R.id.middlename)
    SemiBoldTextView middlename;
    @Bind(R.id.img_right)
    ImageView imgRight;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.Upload_Image)
    RegularTextView UploadImage;
    @Bind(R.id.img_pic)
    ImageView imgPic;
    @Bind(R.id.converted)
    SemiBoldTextView converted;
    @Bind(R.id.converted_to)
    MediumTextView convertedTo;
    @Bind(R.id.Colors)
    MediumTextView Colors;
    @Bind(R.id.Pick_colors)
    SemiBoldTextView PickColors;
    @Bind(R.id.ll_red)
    LinearLayout llRed;
    @Bind(R.id.ll_mehandi)
    LinearLayout llMehandi;
    @Bind(R.id.ll_blue)
    LinearLayout llBlue;
    @Bind(R.id.QR_Code)
    TextView QRCode;
    @Bind(R.id.color1)
    SemiBoldTextView color1;
    @Bind(R.id.color2)
    SemiBoldTextView color2;
    @Bind(R.id.color3)
    SemiBoldTextView color3;
    private Activity activity;

    CommonMethods commanMethods;


    private Data data;
    private String userid;
    private File originalfile, generatedFile;
    private Uri generatedFileUri;
    private String generatedFilePath;
    private Bitmap generatedBitmap;
    private Bitmap originalBitmap;

    private String userChoosenTask;
    private File photofromCamera;
    private Uri mImageUri;
    private Uri picturePath2;
    private String gallerypicturePath;
    private Bitmap photo;

    private String fullimagePath;

    private Bitmap bbmm;

    private boolean RedSelect, Mhandi, Blue;

    private static final int REQUEST_CAMERA = 1;
    private static final int SELECT_FILE = 2;
    private Canvas canvas;
    private Bitmap orignalphoto;
    private QRColorData qrColorDataRed;
    private QRColorData qrColorDataGreen;
    private QRColorData qrColorDataBlue;

    private Paint paintRed;
    private Paint paintGreen;
    private Paint paintBlue;

    private String picturePath;
    private Bitmap croppedBitmap;

    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 123;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_your_code);
        ButterKnife.bind(this);

        setUI();


    }

    private void setUI() {

        activity = this;

        commanMethods = new CommonMethods(activity, this);

        commanMethods.setUpToolbars((AppCompatActivity) activity, "Create your code");


        Gson gson = new Gson();
        data = gson.fromJson(Common.getPreferences(activity, "data"), Data.class);
        userid = data.getUserid();


    }


    @OnClick({R.id.Upload_Image, R.id.img_pic, R.id.Pick_colors, R.id.ll_red, R.id.ll_mehandi, R.id.ll_blue, R.id.color1, R.id.color2, R.id.color3, R.id.QR_Code, R.id.converted_to})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Upload_Image:

                SelectImage();

                break;
            case R.id.img_pic:
                break;
            case R.id.Pick_colors:
                break;
            case R.id.ll_red:


                if (originalBitmap != null) {
                    if (qrColorDataRed == null) {
                        qrColorDataRed = new QRColorData(Color.parseColor("#D96666"));
                        paintRed = new Paint();
                        Random r = new Random();

                        int X = r.nextInt(originalBitmap.getWidth() - 100);
                        int Y = r.nextInt(originalBitmap.getHeight() - 100);


                        QRColorUnitData qrColorUnitData1 = new QRColorUnitData(X, Y + 30, X + 80, Y);
                        QRColorUnitData qrColorUnitData2 = new QRColorUnitData(X - 50, Y, X, Y - 40);
                        QRColorUnitData qrColorUnitData3 = new QRColorUnitData(Y + 20, X, Y, X + 70);
                        QRColorUnitData qrColorUnitData4 = new QRColorUnitData(Y - 40, X, Y, X - 50);
                        QRColorUnitData qrColorUnitData5 = new QRColorUnitData(X + 60, Y, X, Y + 40);

                        qrColorDataRed.setQrColorUnit1(qrColorUnitData1);
                        qrColorDataRed.setQrColorUnit2(qrColorUnitData2);
                        qrColorDataRed.setQrColorUnit3(qrColorUnitData3);
                        qrColorDataRed.setQrColorUnit4(qrColorUnitData4);
                        qrColorDataRed.setQrColorUnit5(qrColorUnitData5);


                        Bitmap bitmap = createNewBitmap(qrColorDataRed, qrColorDataGreen, qrColorDataBlue, originalBitmap, paintRed, paintGreen);
                        imgPic.setImageBitmap(bitmap);

                        generatedBitmap = bitmap;


                    } else {
                        qrColorDataRed = null;
                        Bitmap bitmap = createNewBitmap(qrColorDataRed, qrColorDataGreen, qrColorDataBlue, originalBitmap, paintRed, paintGreen);
                        imgPic.setImageBitmap(bitmap);
                        generatedBitmap = bitmap;

                    }


                } else {
                    Toast.makeText(activity, "Please select image!", Toast.LENGTH_SHORT).show();
                }


                break;
            case R.id.ll_mehandi:


                if (originalBitmap != null) {
                    if (qrColorDataGreen == null) {
                        qrColorDataGreen = new QRColorData(Color.parseColor("#B1C51A"));
                        paintGreen = new Paint();
                        Random r = new Random();
                        int X = r.nextInt(originalBitmap.getWidth() - 100);
                        int Y = r.nextInt(originalBitmap.getHeight() - 100);


                        QRColorUnitData qrColorUnitData1 = new QRColorUnitData(X, Y + 30, X + 80, Y);
                        QRColorUnitData qrColorUnitData2 = new QRColorUnitData(X - 50, Y, X, Y - 40);
                        QRColorUnitData qrColorUnitData3 = new QRColorUnitData(Y + 20, X, Y, X + 70);
                        QRColorUnitData qrColorUnitData4 = new QRColorUnitData(Y - 40, X, Y, X - 50);
                        QRColorUnitData qrColorUnitData5 = new QRColorUnitData(X + 60, Y, X, Y + 40);

                        qrColorDataGreen.setQrColorUnit1(qrColorUnitData1);
                        qrColorDataGreen.setQrColorUnit2(qrColorUnitData2);
                        qrColorDataGreen.setQrColorUnit3(qrColorUnitData3);
                        qrColorDataGreen.setQrColorUnit4(qrColorUnitData4);
                        qrColorDataGreen.setQrColorUnit5(qrColorUnitData5);

                        Bitmap bitmap = createNewBitmap(qrColorDataRed, qrColorDataGreen, qrColorDataBlue, originalBitmap, paintRed, paintGreen);
                        imgPic.setImageBitmap(bitmap);

                        generatedBitmap = bitmap;

                    } else {
                        qrColorDataGreen = null;
                        Bitmap bitmap = createNewBitmap(qrColorDataRed, qrColorDataGreen, qrColorDataBlue, originalBitmap, paintRed, paintGreen);
                        imgPic.setImageBitmap(bitmap);


                        generatedBitmap = bitmap;


                    }

                } else {
                    Toast.makeText(activity, "Please select image!", Toast.LENGTH_SHORT).show();
                }


                break;
            case R.id.ll_blue:


                if (originalBitmap != null) {
                    if (qrColorDataBlue == null) {
                        qrColorDataBlue = new QRColorData(Color.parseColor("#658CD9"));
                        paintBlue = new Paint();
                        Random r = new Random();
                        int X = r.nextInt(originalBitmap.getWidth() - 100);
                        int Y = r.nextInt(originalBitmap.getHeight() - 100);


                        QRColorUnitData qrColorUnitData1 = new QRColorUnitData(X, Y + 30, X + 80, Y);
                        QRColorUnitData qrColorUnitData2 = new QRColorUnitData(X - 50, Y, X, Y - 40);
                        QRColorUnitData qrColorUnitData3 = new QRColorUnitData(Y + 20, X, Y, X + 70);
                        QRColorUnitData qrColorUnitData4 = new QRColorUnitData(Y - 40, X, Y, X - 50);
                        QRColorUnitData qrColorUnitData5 = new QRColorUnitData(X + 60, Y, X, Y + 40);

                        qrColorDataBlue.setQrColorUnit1(qrColorUnitData1);
                        qrColorDataBlue.setQrColorUnit2(qrColorUnitData2);
                        qrColorDataBlue.setQrColorUnit3(qrColorUnitData3);
                        qrColorDataBlue.setQrColorUnit4(qrColorUnitData4);
                        qrColorDataBlue.setQrColorUnit5(qrColorUnitData5);

                        Bitmap bitmap = createNewBitmap(qrColorDataRed, qrColorDataGreen, qrColorDataBlue, originalBitmap, paintRed, paintGreen);
                        imgPic.setImageBitmap(bitmap);

                        generatedBitmap = bitmap;

                    } else {
                        qrColorDataBlue = null;
                        Bitmap bitmap = createNewBitmap(qrColorDataRed, qrColorDataGreen, qrColorDataBlue, originalBitmap, paintRed, paintGreen);
                        imgPic.setImageBitmap(bitmap);


                        generatedBitmap = bitmap;
                    }


                } else {
                    Toast.makeText(activity, "Please select image!", Toast.LENGTH_SHORT).show();
                }


                break;

            case R.id.converted_to:

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (originalBitmap != null) {
                            originalBitmap = createBlackWhiteImage(originalBitmap, 0);
                            Bitmap bitmap = createNewBitmap(qrColorDataRed, qrColorDataGreen, qrColorDataBlue, originalBitmap, paintRed, paintGreen);
                            imgPic.setImageBitmap(bitmap);

                            generatedBitmap = bitmap;
                        } else {
                            Toast.makeText(activity, "Please select image!", Toast.LENGTH_LONG).show();

                        }

                    }
                });


                break;


            case R.id.color1:


                break;
            case R.id.color2:


                break;
            case R.id.color3:


                break;
            case R.id.QR_Code:
                if (Common.getConnectivityStatus(activity)) {
                    if (userValidation()) {
                        new GnerateCodeFromServer().execute();
                    }
                } else {
                    Common.showToast(activity, "Please check your internet connection.");

                }


        }
    }


    private boolean userValidation() {

        if (RedSelect && Mhandi || RedSelect && Blue || Mhandi && Blue) {

            generatedFileUri = getImageUri(activity, generatedBitmap);


            generatedFilePath = (getRealPathFromURI(generatedFileUri));


            generatedFile = new File(generatedFilePath);

            Log.v("imageFile", generatedFileUri + generatedFilePath + generatedFile);
            return true;

        } else {
            Toast.makeText(activity, "Please select more than 1 color", Toast.LENGTH_LONG).show();

            return false;
        }

    }


    private void SelectImage() {


        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = utils.Utility.checkPermission(activity);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    System.out.println("resultpermission" + result);

                    if (ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        // Camera permission has not been granted.

                        requestCameraPermission();
                    }else
                    {cameraIntent();

                    }

                     /*   if (result)
                        cameraIntent();*/
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();


    }

    private void galleryIntent() {

        Intent i = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, SELECT_FILE);


    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            // place where to store camera taken picture
            photofromCamera = this.createTemporaryFile("picture", ".jpg");
            photofromCamera.delete();
        } catch (Exception e) {
            // Log.v(TAG, "Can't create file to take picture!");
            Toast.makeText(activity, "Please check SD card! Image shot is impossible!", Toast.LENGTH_SHORT);
        }
        mImageUri = Uri.fromFile(photofromCamera);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);


        // intent.putExtra(MediaStore.EXTRA_OUTPUT, getOutputMediaFileUri());

        //start camera intent

        startActivityForResult(intent,
                REQUEST_CAMERA);

    }


    private void requestCameraPermission() {

        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                android.Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(activity);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("Camera permission is necessary to scan code");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CAMERA);
        }
        // END_INCLUDE(camera_permission_request)
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case utils.Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
//code for deny
                }
                break;


        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.v("camera response", requestCode + "");

        if (resultCode == 3) {

            picturePath = data.getStringExtra("imgPath");

            Log.v("", "path=" + picturePath);

            croppedBitmap = BitmapFactory.decodeFile(picturePath);

            originalfile = new File(picturePath);

            orignalphoto = Bitmap.createScaledBitmap(croppedBitmap, (int) (612), (int) (612), true);

            originalBitmap = orignalphoto;

            Glide.with(activity).load(picturePath).into(imgPic);


            // UpLoadCroppedPhoto(addPhotoNumber);

        }


        if (resultCode == Activity.RESULT_OK) {

            switch (requestCode) {
                case SELECT_FILE:


                    picturePath2 = data.getData();
                    System.out.println(" gallery  before rotate picture Uri" + picturePath2);
                    String[] filePath = {MediaStore.Images.Media.DATA};

                    Cursor c = activity.getContentResolver().query(picturePath2, filePath, null, null, null);
                    System.out.println(" gallery   before rotate  picture cursor" + picturePath2);
                    c.moveToFirst();

                    int columnIndex = c.getColumnIndex(filePath[0]);

                    gallerypicturePath = c.getString(columnIndex);
                    // fullimagePath = gallerypicturePath;
                    System.out.println(" gallery   before rotate   picture Path" + gallerypicturePath);

                    c.close();


                    /// rotate image code shyam

                    Bitmap gallerybitmap;
                    try {

                        gallerybitmap = BitmapFactory.decodeFile(gallerypicturePath);
                        photo = gallerybitmap;


                    } catch (Exception e) {
                        Toast.makeText(activity, "Failed to load", Toast.LENGTH_SHORT).show();
                    }

                    // fullimagePath= photofromCamera.getAbsolutePath();


                    //rotation for samsung


                    //  picturePath2 = getImageUri(getActivity(), photo);


                    ExifInterface exif = null;

                    Bitmap sourceBitmap = photo;

                    try {
                        exif = new ExifInterface(gallerypicturePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    System.out.println("in gallery  rotate back and ori" + orientation);
                    if (orientation == 6) {
                        Matrix matrix = new Matrix();
                        matrix.postRotate(90);
                        photo = Bitmap.createBitmap(sourceBitmap, 0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight(),
                                matrix, true);
                    } else if (orientation == 8) {
                        Matrix matrix = new Matrix();
                        matrix.postRotate(270);
                        photo = Bitmap.createBitmap(sourceBitmap, 0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight(),
                                matrix, true);
                    } else {
                        photo = sourceBitmap;
                    }

                    picturePath2 = null;
                    picturePath2 = getImageUri(activity, photo);


                    gallerypicturePath = (getRealPathFromURI(picturePath2));

                    fullimagePath = gallerypicturePath;


                    // fullimagePath = camerapicturePath;

                 /*   orignalphoto = Bitmap.createScaledBitmap(photo, (int) (612), (int) (612), true);

                    originalBitmap = orignalphoto;

                    originalfile = new File(fullimagePath);

*/
                    System.out.println("gallery pic   after rotate  Uri" + picturePath2);
                    System.out.println("gallery pic after rotate   picturePath2" + gallerypicturePath);


                    Intent intent = new Intent(activity,
                            CropImage.class);
                    intent.putExtra("image-path", fullimagePath);
                    intent.putExtra("scale", true);
                    intent.putExtra("circleCrop", false);
                    intent.putExtra("return-data", false);
                    this.startActivityForResult(intent, 3);


                    //   Glide.with(activity).load(fullimagePath).into(imgPic);


                    break;

                case REQUEST_CAMERA:

                    Bitmap bitmap;
                    try {

                        bitmap = BitmapFactory.decodeFile(photofromCamera.getAbsolutePath());


                        photo = bitmap;


                    } catch (Exception e) {
                        Toast.makeText(activity, "Failed to load", Toast.LENGTH_SHORT).show();
                    }

                    ExifInterface exif2 = null;

                    Bitmap sourceBitmap2 = photo;

                    try {
                        exif2 = new ExifInterface(photofromCamera.getAbsolutePath());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    int orientation2 = exif2.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    System.out.println("in camera rotate back and ori" + orientation2);
                    if (orientation2 == 6) {
                        Matrix matrix = new Matrix();
                        matrix.postRotate(90);
                        photo = Bitmap.createBitmap(sourceBitmap2, 0, 0, sourceBitmap2.getWidth(), sourceBitmap2.getHeight(),
                                matrix, true);
                    } else if (orientation2 == 8) {
                        Matrix matrix = new Matrix();
                        matrix.postRotate(270);
                        photo = Bitmap.createBitmap(sourceBitmap2, 0, 0, sourceBitmap2.getWidth(), sourceBitmap2.getHeight(),
                                matrix, true);
                    } else {
                        photo = sourceBitmap2;
                    }

                    picturePath2 = null;
                    picturePath2 = getImageUri(activity, photo);


                    fullimagePath = (getRealPathFromURI(picturePath2));


                    // fullimagePath = camerapicturePath;
                    System.out.println("camerra pic Uri" + picturePath2);
                    System.out.println("camerra pic picturePath2" + fullimagePath);

/*
                    orignalphoto = Bitmap.createScaledBitmap(photo, (int) (612), (int) (612), true);


                    originalBitmap = orignalphoto;

                    originalfile = new File(fullimagePath);

                    Glide.with(activity).load(originalfile).into(imgPic);*/


                    Intent intent2 = new Intent(activity,
                            CropImage.class);
                    intent2.putExtra("image-path", fullimagePath);
                    intent2.putExtra("scale", true);
                    intent2.putExtra("circleCrop", false);
                    intent2.putExtra("return-data", false);
                    this.startActivityForResult(intent2, 3);


                    break;
            }


        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        //  inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }


    private File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdir();
        }
        return File.createTempFile(part, ext, tempDir);
    }


    public static Bitmap createBlackWhiteImage(Bitmap src, double value) {

        int width, height;
        height = src.getHeight();
        width = src.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(src, 0, 0, paint);
        return bmpGrayscale;

    }


    private Bitmap createNewBitmap(QRColorData qrColorDataRed, QRColorData qrColorDataGreen, QRColorData qrColorDataBlue, Bitmap originalBitmap, Paint paintRed, Paint paintGreen) {

        Bitmap resultBitmap = Bitmap.createBitmap(originalBitmap.getWidth(), originalBitmap.getHeight(), originalBitmap.getConfig());
        canvas = new Canvas(resultBitmap);
        canvas.drawBitmap(originalBitmap, new Matrix(), null);


        if (paintRed != null) {
            if (qrColorDataRed != null) {

                color1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);

                RedSelect = true;

                paintRed.setColor(Color.parseColor("#D96666"));

                Log.v("Image width", originalBitmap.getWidth() + "");
                Log.v("Image height", originalBitmap.getHeight() + "");

                Random r = new Random();
                int X = r.nextInt(originalBitmap.getWidth() - 100);
                int Y = r.nextInt(originalBitmap.getHeight() - 100);

                Log.v("Image X", X + "");
                Log.v("Image Y", Y + "");


//original
                canvas.drawRect(qrColorDataRed.getQrColorUnit1().startX, qrColorDataRed.getQrColorUnit1().startY, qrColorDataRed.getQrColorUnit1().height, qrColorDataRed.getQrColorUnit1().width, paintRed);  // horizontal lineP
                canvas.drawRect(qrColorDataRed.getQrColorUnit2().startX, qrColorDataRed.getQrColorUnit2().startY, qrColorDataRed.getQrColorUnit2().height, qrColorDataRed.getQrColorUnit2().width, paintRed);  // horizontal lineP
                canvas.drawRect(qrColorDataRed.getQrColorUnit3().startX, qrColorDataRed.getQrColorUnit3().startY, qrColorDataRed.getQrColorUnit3().height, qrColorDataRed.getQrColorUnit3().width, paintRed);  // horizontal lineP
                canvas.drawRect(qrColorDataRed.getQrColorUnit4().startX, qrColorDataRed.getQrColorUnit4().startY, qrColorDataRed.getQrColorUnit4().height, qrColorDataRed.getQrColorUnit4().width, paintRed);  // horizontal lineP
                canvas.drawRect(qrColorDataRed.getQrColorUnit5().startX, qrColorDataRed.getQrColorUnit5().startY, qrColorDataRed.getQrColorUnit5().height, qrColorDataRed.getQrColorUnit5().width, paintRed);  // horizontal lineP


                // photo = resultBitmap;

                //   generatedBitmap = resultBitmap;


            } else {

                RedSelect = false;

                color1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.circle, 0, 0, 0);
                paintRed.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

                canvas.drawRect(0, 0, 0, 0, paintRed);

            }

        }

        if (paintGreen != null) {
            if (qrColorDataGreen != null) {

                color2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
                Mhandi = true;

                paintGreen.setColor(Color.parseColor("#B1C51A"));

                Log.v("Image width", originalBitmap.getWidth() + "");
                Log.v("Image height", originalBitmap.getHeight() + "");

                Random r = new Random();
                int X = r.nextInt(originalBitmap.getWidth() - 100);
                int Y = r.nextInt(originalBitmap.getHeight() - 100);

                Log.v("Image X", X + "");
                Log.v("Image Y", Y + "");


//original
                canvas.drawRect(qrColorDataGreen.getQrColorUnit1().startX, qrColorDataGreen.getQrColorUnit1().startY, qrColorDataGreen.getQrColorUnit1().height, qrColorDataGreen.getQrColorUnit1().width, paintGreen);  // horizontal lineP
                canvas.drawRect(qrColorDataGreen.getQrColorUnit2().startX, qrColorDataGreen.getQrColorUnit2().startY, qrColorDataGreen.getQrColorUnit2().height, qrColorDataGreen.getQrColorUnit2().width, paintGreen);  // horizontal lineP
                canvas.drawRect(qrColorDataGreen.getQrColorUnit3().startX, qrColorDataGreen.getQrColorUnit3().startY, qrColorDataGreen.getQrColorUnit3().height, qrColorDataGreen.getQrColorUnit3().width, paintGreen);  // horizontal lineP
                canvas.drawRect(qrColorDataGreen.getQrColorUnit4().startX, qrColorDataGreen.getQrColorUnit4().startY, qrColorDataGreen.getQrColorUnit4().height, qrColorDataGreen.getQrColorUnit4().width, paintGreen);  // horizontal lineP
                canvas.drawRect(qrColorDataGreen.getQrColorUnit5().startX, qrColorDataGreen.getQrColorUnit5().startY, qrColorDataGreen.getQrColorUnit5().height, qrColorDataGreen.getQrColorUnit5().width, paintGreen);  // horizontal lineP


                // photo = resultBitmap;

                //   generatedBitmap = resultBitmap;


            } else {

                Mhandi = false;

                color2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.circle, 0, 0, 0);
                paintGreen.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

                canvas.drawRect(0, 0, 0, 0, paintGreen);

            }
        }


        if (paintBlue != null) {
            if (qrColorDataBlue != null) {

                color3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);

                Blue = true;
                paintBlue.setColor(Color.parseColor("#658CD9"));

                Log.v("Image width", originalBitmap.getWidth() + "");
                Log.v("Image height", originalBitmap.getHeight() + "");

                Random r = new Random();
                int X = r.nextInt(originalBitmap.getWidth() - 100);
                int Y = r.nextInt(originalBitmap.getHeight() - 100);

                Log.v("Image X", X + "");
                Log.v("Image Y", Y + "");


//original
                canvas.drawRect(qrColorDataBlue.getQrColorUnit1().startX, qrColorDataBlue.getQrColorUnit1().startY, qrColorDataBlue.getQrColorUnit1().height, qrColorDataBlue.getQrColorUnit1().width, paintBlue);  // horizontal lineP
                canvas.drawRect(qrColorDataBlue.getQrColorUnit2().startX, qrColorDataBlue.getQrColorUnit2().startY, qrColorDataBlue.getQrColorUnit2().height, qrColorDataBlue.getQrColorUnit2().width, paintBlue);  // horizontal lineP
                canvas.drawRect(qrColorDataBlue.getQrColorUnit3().startX, qrColorDataBlue.getQrColorUnit3().startY, qrColorDataBlue.getQrColorUnit3().height, qrColorDataBlue.getQrColorUnit3().width, paintBlue);  // horizontal lineP
                canvas.drawRect(qrColorDataBlue.getQrColorUnit4().startX, qrColorDataBlue.getQrColorUnit4().startY, qrColorDataBlue.getQrColorUnit4().height, qrColorDataBlue.getQrColorUnit4().width, paintBlue);  // horizontal lineP
                canvas.drawRect(qrColorDataBlue.getQrColorUnit5().startX, qrColorDataBlue.getQrColorUnit5().startY, qrColorDataBlue.getQrColorUnit5().height, qrColorDataBlue.getQrColorUnit5().width, paintBlue);  // horizontal lineP


                // photo = resultBitmap;

                //   generatedBitmap = resultBitmap;


            } else {


                Blue = false;

                color3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.circle, 0, 0, 0);
                paintBlue.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

                canvas.drawRect(0, 0, 0, 0, paintBlue);

            }
        }


        return resultBitmap;
    }


    public class GnerateCodeFromServer extends AsyncTask<String, Void, String> {
        String response = "";
        ProgressDialog mprogressDialog;
        private String strMsg;
        private String strStatus;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            mprogressDialog = new ProgressDialog(activity);
            mprogressDialog.setMessage("Please wait...");
            mprogressDialog.setCancelable(false);
            mprogressDialog.show();
        }

        @Override
        @SuppressWarnings("deprecation")
        protected String doInBackground(String... params) {
            final Bundle bundleForProfile = getPhotoCommentBundle(generatedFile, originalfile, userid);


            String url = "http://employeetracker.us/somerge/services/ws-user.php?type=STEP-THREE";

            try {
                response = Utility.openUrll(url, "POST", bundleForProfile);

                JSONObject json_obj_response = new JSONObject(response);
                strMsg = json_obj_response.getString("msg");
                strStatus = json_obj_response.getString("status");

                System.out.println("result >>>>>>>>>>> " + response);

            } catch (Exception e) {

                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            mprogressDialog.dismiss();

            System.out.println("result  response" + result);
            JSONObject jsonData = null;

            if (strStatus.equals("true")) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getJSONObject("data") != null) {
                        jsonData = jsonObject.getJSONObject("data");

                        Common.SetPreferences(activity, "userid", jsonData.getString("userid"));


                        Common.SetPreferences(activity, "data", jsonData.toString());

                        Intent intent = new Intent(activity, CommanFragmentActitvty.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                        finish();


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {

                Toast.makeText(activity, "Please try again", Toast.LENGTH_LONG).show();
            }


        }

    }


    public Bundle getPhotoCommentBundle(File generatedFile, File originalfile, String userid) {


        Log.v("originalPath", originalfile + "");

        Bundle bundle_before_send = new Bundle();


        byte[] generatedFile_byte_array = getByteArray(generatedFile);
        byte[] originalfile_byte_array = getByteArray(originalfile);


        bundle_before_send.putByteArray("original_pic", originalfile_byte_array);
        bundle_before_send.putByteArray("generated_pic", generatedFile_byte_array);


        bundle_before_send.putString("userid", userid);

        System.out.println("bundle before sending " + bundle_before_send);

        return bundle_before_send;
    }


    private byte[] getByteArray(File file) {
        int maxBufferSize = 1 * 1024 * 1024;
        int bytesAvailable = 0;
        int bufferSize = Math.max(bytesAvailable, maxBufferSize);
        byte[] buffer = new byte[bufferSize];

        boolean status = false;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        System.out.println("alFile.get(i).getAbsolutePath()" + file);
        bbmm = BitmapFactory.decodeFile(file.getAbsolutePath());
        if (bbmm != null) {
            status = bbmm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        } else {
            System.out.println("Your bitmap was null");
        }

        buffer = stream.toByteArray();


        return buffer;
    }

}

