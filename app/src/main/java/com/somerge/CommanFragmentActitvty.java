package com.somerge;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import Fragment.HomeFragment;
import butterknife.ButterKnife;
import follow.instagram.connection.FollowOnInstagram;

/**
 * Created by user7 on 18/10/16.
 */
public class CommanFragmentActitvty extends AppCompatActivity {

    private Activity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);


        setContentView(R.layout.coman_activty);

        ButterKnife.bind(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setUI();


    }

    private void setUI() {

        activity = this;


        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.right_in, R.anim.left_out, R.anim.left_in, R.anim.right_out)
                .replace(R.id.frame_container, new HomeFragment()).commit();


    }


    public void Loadfragment(Fragment loadfragment) {


        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.right_in, R.anim.left_out, R.anim.left_in, R.anim.right_out)
                .replace(R.id.frame_container, loadfragment).addToBackStack(null).
                commit();


    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        if (fragment instanceof HomeFragment) {

            finish();

        } else {
            super.onBackPressed();

        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(FollowOnInstagram.mProgress != null)
        {
            FollowOnInstagram.mProgress.dismiss();
            FollowOnInstagram.mProgress= null;
        }
    }
}
