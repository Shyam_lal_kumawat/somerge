package com.somerge;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gettersetter.Data;
import io.fabric.sdk.android.Fabric;
import utils.Common;

public class SplashActivity extends AppCompatActivity implements Animator.AnimatorListener {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.

    private static final String TWITTER_KEY = "hqK7xdX9oEqyKI7fpoORQfx5n"; // "vQhEDS9JOYysgQFhU3XggA3NY";
    private static final String TWITTER_SECRET = "6OgpGYiD2T9NK4uhXmshirwcxgeDbw48i6nTVp0tPdg1XKxTcN";//"veNW
    /*
    private static final String TWITTER_KEY = "vQhEDS9JOYysgQFhU3XggA3NY";
    private static final String TWITTER_SECRET = "veNWQKmGUlLWYbw9nU1dJhKQqwqCa8HYWmiSFdZ5KmlsBfrspm";
*/


    @Bind(R.id.logo)
    ImageView logo;
    private Animation fadeIn;
    private AppCompatActivity activity;
    private Data data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }


        setBodyUI();

    }

    private void setBodyUI() {


        activity = this;
        fadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);

        logo.startAnimation(fadeIn);


        String userid = getSharedPreferences("prefs_login", Context.MODE_PRIVATE).getString("userid", "");
        if (!userid.equals("")) {
            Gson gson = new Gson();
            data = gson.fromJson(Common.getPreferences(this, "data"), Data.class);
        }

        Thread timerThread = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {


                    if (data != null) {
                    /*    if (data.getStep_completed().equals("1")) {
                            Intent intent = new Intent(SplashActivity.this, BasicDetailActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                            finish();
                        } else if (data.getStep_completed().equals("2")) {

                        }*/


                        if (data.getEmail().equals("") || (data.getFirst_name().equals("")) || (data.getLast_name().equals("")) || (data.getMobile().equals(""))) {
                            Intent intent = new Intent(SplashActivity.this, BasicDetailActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                            finish();
                        } else if (data.getQr_image().equals("")) {
                            Intent intent = new Intent(SplashActivity.this, CreateQRCodeActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                            finish();

                        } else {

                            Intent intent = new Intent(SplashActivity.this, CommanFragmentActitvty.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                            finish();
                        }


                    } else {
                        Intent intent = new Intent(activity, LoginActivity.class);
                        startActivity(intent);
                        finish();

                        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                    }
                }
            }
        };
        timerThread.start();


    }

    @Override
    public void onAnimationStart(Animator animator) {

    }

    @Override
    public void onAnimationEnd(Animator animator) {

    }

    @Override
    public void onAnimationCancel(Animator animator) {

    }

    @Override
    public void onAnimationRepeat(Animator animator) {

    }


    @OnClick(R.id.logo)
    public void onClick() {
    }
}
