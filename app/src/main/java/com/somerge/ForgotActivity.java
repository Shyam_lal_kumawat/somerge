package com.somerge;

import android.app.Activity;
import android.os.Bundle;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import request.BaseRequestData;
import request.Constant;
import request.RequestedServiceDataModel;
import request.ResponseDelegate;
import request.ResponseType;
import utils.Common;
import utils.LightEditText;
import utils.RegularTextView;

/**
 * Created by user7 on 18/10/16.
 */
public class ForgotActivity extends Activity implements ResponseDelegate {


    @Bind(R.id.et_email)
    LightEditText etEmail;
    @Bind(R.id.submit)
    RegularTextView submit;
    private Activity activity;
    private RequestedServiceDataModel requestedServiceDataModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot);
        ButterKnife.bind(this);

        setUI();


    }

    private void setUI() {

        activity = this;


    }

    @OnClick(R.id.submit)
    public void onClick() {
        checkValidation();

//        activity.onBackPressed();
    }

    private void checkValidation() {
        if (etEmail.getText().toString().trim().equalsIgnoreCase("")) {
            Common.showToast(this, "Please enter email address.");
        } else if (!Common.isValidEmail(etEmail.getText().toString().trim())) {
            Common.showToast(this, "Please enter valid email address.");
        } else {
            serverRequestForForgetPassword();
        }
    }

    private void serverRequestForForgetPassword() {
        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.FORGOT);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("email", etEmail.getText().toString().trim());
        requestedServiceDataModel.setWebServiceType("FORGOT");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String jsondata, String message, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.FORGOT:
                Common.showToast(activity, message);
                onBackPressed();

                break;
        }
    }

    @Override
    public void onFailure(String jsondata, String message, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.FORGOT:
                Common.showToast(activity, message);

                break;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
