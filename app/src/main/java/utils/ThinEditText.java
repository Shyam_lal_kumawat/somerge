package utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class ThinEditText extends EditText {
    private Context c;

    public ThinEditText(Context c) {
        super(c);
        this.c = c;
        Typeface tfs = Typeface.createFromAsset(c.getAssets(),
                "latothin.ttf");
        setTypeface(tfs);

    }

    public ThinEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.c = context;
        Typeface tfs = Typeface.createFromAsset(c.getAssets(),
                "latothin.ttf");
        setTypeface(tfs);
        // TODO Auto-generated constructor stub
    }

    public ThinEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.c = context;
        Typeface tfs = Typeface.createFromAsset(c.getAssets(),
                "latothin.ttf");
        setTypeface(tfs);

    }
}
