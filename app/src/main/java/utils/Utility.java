package utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Set;

public class Utility {

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public static String encodePostBody(Bundle parameters, String boundary) {
        if (parameters == null)
            return "";
        StringBuilder sb = new StringBuilder();

        for (String key : parameters.keySet()) {
            if (parameters.getByteArray(key) != null) {
                continue;
            }

            sb.append("Content-Disposition: form-data; name=\"" + key
                    + "\"\r\n\r\n" + parameters.getString(key));
            sb.append("\r\n" + "--" + boundary + "\r\n");
        }

        return sb.toString();
    }


    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String encodeUrl(Bundle parameters) {
        if (parameters == null) {
            return "";
        }

        JSONObject json = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        Set<String> keys = parameters.keySet();
        for (String key : keys) {
            try {
                // json.put(key, bundle.get(key)); see edit below
                json.put(key, JSONObject.wrap(parameters.get(key)));
                jsonArray.put(json);
            } catch (JSONException e) {
                //Handle exception here
            }
        }
        return jsonArray.toString();

//        StringBuilder sb = new StringBuilder();
//        boolean first = true;
//        for (String key : parameters.keySet()) {
//            if (first)
//                first = false;
//            else
//                sb.append("&");
//            sb.append(URLEncoder.encode(key) + "="
//                    + URLEncoder.encode(parameters.getString(key)));
//        }
//
//        System.out.print("sb value" + sb.toString());
//        return sb.toString();
    }

    public static String openUrll(String url, String method, Bundle params)
            throws MalformedURLException, IOException {
        System.out.println("step 11");
        // random string as boundary for multi-part http post
        String strBoundary = "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f";
        String endLine = "\r\n";

        OutputStream os = null;

        if (method.equals("GET")) {
            url = url + "&data=" + encodeUrl(params);
        }
        android.util.Log.d("Facebook-Util", method + " URL: " + url);

        // Open a HTTP connection to the URL
        HttpURLConnection conn = (HttpURLConnection) new URL(url)
                .openConnection();

        conn.setConnectTimeout(20000);

		/* conn.setChunkedStreamingMode(1024*4); */

        conn.setRequestProperty("User-Agent", System.getProperties()
                .getProperty("http.agent") + " FacebookAndroidSDK");
        try {
            if (!method.equals("GET")) {
                Bundle dataparams = new Bundle();

                for (String key : params.keySet()) {
                    System.out.println("params" + params + "key" + key);
                    if (params.getByteArray(key) != null) {
                        dataparams.putByteArray(key, params.getByteArray(key));
                        System.out.println("byte array:"
                                + params.getByteArray(key));
                    } else {
                        dataparams.putString(key, params.getString(key));
                    }
                }

                // use method override
//				if (!params.containsKey("method")) {
//					params.putString("method", method);
//				}

                if (params.containsKey("access_token")) {
                    String decoded_token = URLDecoder.decode(params
                            .getString("access_token"));
                    params.putString("access_token", decoded_token);
                }

                // Use a post method.
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type",
                        "multipart/form-data;boundary=" + strBoundary);

                // Allow Outputs
                conn.setDoOutput(true);

                // Allow Inputs
                conn.setDoInput(true);
                System.out.println("step 12");
                conn.setRequestProperty("Connection", "Keep-Alive");
                System.out.println("step 13");
                conn.connect();
                System.out.println("step 14");
                // System.out.println(params.getString("input"));
                os = new BufferedOutputStream(conn.getOutputStream());

                // os.write((params.getString("input")).getBytes());

                System.out.println("step 15");
                os.write(("--" + strBoundary + endLine).getBytes());
                os.write((encodePostBody(params, strBoundary)).getBytes());
                os.write((endLine + "--" + strBoundary + endLine).getBytes());

                if (!dataparams.isEmpty()) {
                    System.out.println("step 16");
                    for (String key : dataparams.keySet()) {
                        System.out.println("key is:" + key);

                        if (key.equals("original_pic") || key.equals("generated_pic")) {
                            os.write(("Content-Disposition: form-data; name=\""
                                    + key + "\"; filename=\"" + key + ".jpeg"
                                    + "\"" + endLine).getBytes());
                            os.write(("Content-Type: application/octet-stream"
                                    + endLine + endLine).getBytes());
                            os.write(dataparams.getByteArray(key));
                        } else {
                            os.write(("Content-Disposition: form-data; name=\""
                                    + key + "\"" + endLine).getBytes());
                            os.write(("Content-Type: content/unknown" + endLine + endLine)
                                    .getBytes());
                            os.write(dataparams.getString(key).getBytes());
                        }
                        // os.write();
                        os.write((endLine + "--" + strBoundary + endLine)
                                .getBytes());
                        // os.flush();
                    }
                }
                os.flush();
                System.out.println("step 17");
                dataparams.clear();

            }
            System.out.println("step 18");
            String response = "";
            try {
                System.out.println("step 19");
                response = read(conn.getInputStream());

                System.out.println("step 19A");

            } catch (FileNotFoundException e) {
                System.out.println("step 20");
                // Error Stream contains JSON that we can parse to a FB error
                response = read(conn.getErrorStream());
            }

            System.out.println("response is : " + response);
            conn.disconnect();

            return response;
        } catch (OutOfMemoryError e) {
            if (os != null)
                os.flush();
            conn.disconnect();
            e.printStackTrace();
            return null;
        }
    }

    private static String read(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(in), 1000);
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            sb.append(line);
        }
        in.close();
        return sb.toString();
    }
}