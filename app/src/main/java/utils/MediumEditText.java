package utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class MediumEditText extends EditText {
    private Context c;

    public MediumEditText(Context c) {
        super(c);
        this.c = c;
        Typeface tfs = Typeface.createFromAsset(c.getAssets(),
                "latomedium.ttf");
        setTypeface(tfs);

    }

    public MediumEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.c = context;
        Typeface tfs = Typeface.createFromAsset(c.getAssets(),
                "latomedium.ttf");
        setTypeface(tfs);
        // TODO Auto-generated constructor stub
    }

    public MediumEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.c = context;
        Typeface tfs = Typeface.createFromAsset(c.getAssets(),
                "latomedium.ttf");
        setTypeface(tfs);

    }
}
