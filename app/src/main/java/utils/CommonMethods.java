package utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.somerge.CommanFragmentActitvty;
import com.somerge.R;

import Fragment.CreateCodeFragment;
import Fragment.EditProfileFragment;
import Fragment.SearchFragment;

/**
 * Created by user7 on 19/10/16.
 */

public class CommonMethods {

    Context context;
    private Activity _act;
    private Object listener;
    AppCompatActivity _acActivity;


    @SuppressWarnings("static-access")
    public CommonMethods(Activity activity, Object listener) {
        this.context = activity;
        this._act = activity;
        this.listener = listener;


    }

    @SuppressWarnings("static-access")
    public CommonMethods(AppCompatActivity activity, Object listener) {
        this.context = activity;
        this._act = activity;
        this.listener = listener;


    }

      /*

    Method to Hide KeyBoard.
     */

    public void hideKeyboard() {
        View view = _acActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) _acActivity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }//hide keyboard


    public void setUpToolbars(final AppCompatActivity _acActivity, final String fragment_name) {


        final ImageView img_left, img_right;
        final SemiBoldTextView middlename, left_text;


        this._acActivity = _acActivity;
        Toolbar toolbar = (Toolbar) ((AppCompatActivity) _acActivity).findViewById(R.id.toolbar);

        _acActivity.setSupportActionBar(toolbar);
        _acActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        _acActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        _acActivity.getSupportActionBar().setHomeButtonEnabled(true);


        toolbar.setVisibility(View.VISIBLE);


        img_left = (ImageView) toolbar.findViewById(R.id.img_left);
        middlename = (SemiBoldTextView) toolbar.findViewById(R.id.middlename);
        img_right = (ImageView) toolbar.findViewById(R.id.img_right);


        img_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _acActivity.onBackPressed();
            }
        });

        img_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                FragmentActivity fragmentActivity = (FragmentActivity) _acActivity;
                if (fragment_name.equalsIgnoreCase("SoMerge") || fragment_name.equalsIgnoreCase("Basic Info") || fragment_name.equalsIgnoreCase("Create edit")) {
                    ((CommanFragmentActitvty) _acActivity).Loadfragment(new SearchFragment());
                } else if (fragment_name.equalsIgnoreCase("My Account")) {
                    ((CommanFragmentActitvty) _acActivity).Loadfragment(new EditProfileFragment());
                } else if (fragment_name.equalsIgnoreCase("edit")) {
                    ((CommanFragmentActitvty) _acActivity).Loadfragment(new CreateCodeFragment());

                }
            }
        });


        if (fragment_name.equalsIgnoreCase("Create your code")) {
            // img_right.setVisibility(View.VISIBLE);

            middlename.setText(fragment_name);
            img_left.setVisibility(View.VISIBLE);


        } else if (fragment_name.equalsIgnoreCase("SoMerge")) {
            img_right.setVisibility(View.VISIBLE);

            middlename.setText(fragment_name);
            img_left.setVisibility(View.INVISIBLE);
            img_right.setImageDrawable(_acActivity.getResources().getDrawable(R.drawable.search_white));

        } else if (fragment_name.equalsIgnoreCase("My Account")) {
            middlename.setText(fragment_name);
            img_left.setVisibility(View.VISIBLE);
            img_right.setVisibility(View.VISIBLE);
            img_right.setImageDrawable(_acActivity.getResources().getDrawable(R.drawable.icon_edit));

        } else if (fragment_name.equalsIgnoreCase("Your Connections")) {
            middlename.setText(fragment_name);
            img_left.setVisibility(View.VISIBLE);
            img_right.setVisibility(View.INVISIBLE);

        } else if (fragment_name.equalsIgnoreCase("Search Users")) {
            middlename.setText(fragment_name);
            img_left.setVisibility(View.VISIBLE);
            img_right.setVisibility(View.INVISIBLE);


        } else if (fragment_name.equalsIgnoreCase("edit")) {
            middlename.setText("Basic Info");
            img_left.setVisibility(View.VISIBLE);
            img_right.setVisibility(View.VISIBLE);
            img_right.setImageDrawable(_acActivity.getResources().getDrawable(R.drawable.icon_camera));


        } else if (fragment_name.equalsIgnoreCase("Create edit")) {

            middlename.setText("Create your code");

            img_left.setVisibility(View.VISIBLE);

            img_right.setVisibility(View.VISIBLE);
            img_right.setImageDrawable(_acActivity.getResources().getDrawable(R.drawable.search_white));


        } else if (fragment_name.equalsIgnoreCase("Basic Info")) {

            middlename.setText("Basic Info");

//            img_left.setVisibility(View.VISIBLE);

            img_right.setVisibility(View.INVISIBLE);


        } else if (fragment_name.equalsIgnoreCase("Change Password")) {

            middlename.setText("Change Password");

            img_left.setVisibility(View.VISIBLE);

            img_right.setVisibility(View.INVISIBLE);


        } else if (fragment_name.equalsIgnoreCase("Scan Code")) {
            img_right.setVisibility(View.INVISIBLE);
            middlename.setText(fragment_name);
            img_left.setVisibility(View.INVISIBLE);

        }


    }

    public void Public_Alert(Activity actt) {
        final Dialog dialog;

        dialog = new Dialog(actt, R.style.DialogSlideAnim);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.setContentView(R.layout.alert_public);
        actt.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        dialog.findViewById(R.id.OK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

}
