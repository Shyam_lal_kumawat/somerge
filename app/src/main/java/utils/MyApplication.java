package utils;

import android.app.Application;

import com.somerge.R;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * Created by Shyam lal kumawat on 15/6/16.
 */
@ReportsCrashes( // will not be used
        mailTo = "shyam.k@ninehertzindia.com", // my email here
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crashreport)
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);

    }
}
