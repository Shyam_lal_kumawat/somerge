package utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class SemiBoldTextView extends TextView {
    private Context c;

    public SemiBoldTextView(Context c) {
        super(c);
        this.c = c;
        Typeface tfs = Typeface.createFromAsset(c.getAssets(),
                "latosemibold.ttf");
        setTypeface(tfs);

    }

    public SemiBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.c = context;
        Typeface tfs = Typeface.createFromAsset(c.getAssets(),
                "latosemibold.ttf");
        setTypeface(tfs);
        // TODO Auto-generated constructor stub
    }

    public SemiBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.c = context;
        Typeface tfs = Typeface.createFromAsset(c.getAssets(),
                "latosemibold.ttf");
        setTypeface(tfs);

    }
}
