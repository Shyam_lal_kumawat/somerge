package adpter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.somerge.CommanFragmentActitvty;
import com.somerge.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import Fragment.OtherAccount;
import gettersetter.SearchResult;
import request.Constant;
import utils.CircleTransform;
import utils.MediumTextView;


/**
 * Created by Shyam on 10/18/2016.
 */
public class Search_users_Adapter extends RecyclerView.Adapter<Search_users_Adapter.ViewHolder> {

    private final ArrayList<SearchResult.Detail> al;
    private Activity activity;

    public Search_users_Adapter(Activity activity, ArrayList<SearchResult.Detail> detail) {
        this.activity = activity;
        this.al = detail;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_users_raw, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {


        holder.fname.setText(al.get(position).getFirst_name() + " " + al.get(position).getLast_name());
        holder.username.setText(al.get(position).getUsername());


        Picasso.with(activity).load(Constant.imageBaseUrl + al.get(position).getGenerated_image()).transform(new CircleTransform())
                .placeholder(R.drawable.user_circle).into(holder.img_pic);

        holder.rl_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                OtherAccount otherAccount = new OtherAccount();


                Bundle b = new Bundle();
                b.putString("userid", al.get(position).getUserid());

                otherAccount.setArguments(b);

                ((CommanFragmentActitvty) activity).Loadfragment(otherAccount);

            }
        });


    }


    @Override
    public int getItemCount() {
        return al.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        private final MediumTextView fname;
        private RelativeLayout rl_top;
        private MediumTextView username;
        private ImageView img_pic;

        public ViewHolder(View itemView) {
            super(itemView);

            username = (MediumTextView) itemView.findViewById(R.id.username);
            fname = (MediumTextView) itemView.findViewById(R.id.fname);

            img_pic = (ImageView) itemView.findViewById(R.id.img_pic);
            rl_top = (RelativeLayout) itemView.findViewById(R.id.rl_top);


        }
    }
}
