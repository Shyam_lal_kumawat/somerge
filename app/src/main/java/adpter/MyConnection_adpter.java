package adpter;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.somerge.CommanFragmentActitvty;
import com.somerge.R;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import Fragment.OtherAccount;
import gettersetter.SearchResult;
import utils.Common;
import utils.RegularTextView;
import utils.SemiBoldTextView;


/**
 * Created by Shyam on 10/18/2016.
 */
public class MyConnection_adpter extends RecyclerView.Adapter<MyConnection_adpter.ViewHolder> implements View.OnClickListener {

    private final ArrayList<SearchResult.Detail> alllist;
    private ArrayList<SearchResult.Detail> arraylist;
    private Activity activity;


    private boolean isFB, isInsta, isTwitter, isSnap, is_Profile;
    private ViewHolder vholder;
    private Dialog dialog;


    public MyConnection_adpter(Activity activity, ArrayList<SearchResult.Detail> detail) {

        this.activity = activity;
        this.alllist = detail;

        this.arraylist = new ArrayList<SearchResult.Detail>();
        this.arraylist.addAll(detail);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.your_connection_raw, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        vholder = holder;

        vholder.username.setText(alllist.get(position).getUsername());
        vholder.fname.setText(alllist.get(position).getFirst_name() + " " + alllist.get(position).getLast_name());


        if (alllist.get(position).getFacebook().equals("1")) {

            vholder.btn_Fb.setImageDrawable(activity.getResources().getDrawable(R.drawable.tick));

        } else {
            vholder.btn_Fb.setImageDrawable(activity.getResources().getDrawable(R.drawable.circle));
        }

        if (alllist.get(position).getInstagram().equals("1")) {

            vholder.btn_Instagram.setImageDrawable(activity.getResources().getDrawable(R.drawable.tick));

        } else {
            vholder.btn_Instagram.setImageDrawable(activity.getResources().getDrawable(R.drawable.circle));
        }


        if (alllist.get(position).getTwitter().equals("1")) {

            vholder.btn_Twitter.setImageDrawable(activity.getResources().getDrawable(R.drawable.tick));

        } else {
            vholder.btn_Twitter.setImageDrawable(activity.getResources().getDrawable(R.drawable.circle));
        }


        if (alllist.get(position).getSnapchat().equals("1")) {

            vholder.btn_Snapchat.setImageDrawable(activity.getResources().getDrawable(R.drawable.tick));

        } else {
            vholder.btn_Snapchat.setImageDrawable(activity.getResources().getDrawable(R.drawable.circle));
        }


        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (holder.ll_browsers.getVisibility() == View.VISIBLE) {
                    holder.ll_browsers.setVisibility(View.GONE);
                    holder.imageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.add));

                } else {
                    holder.ll_browsers.setVisibility(View.VISIBLE);
                    holder.imageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.sub));
                }

            }
        });

        holder.Add_to_Contacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ShowContactSaveDialog(alllist, position);
                // Public_Alert();
            }
        });

        holder.username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (alllist.get(position).getUserid() != null) {
                    OtherAccount otherAccount = new OtherAccount();


                    Bundle b = new Bundle();
                    b.putString("userid", alllist.get(position).getUserid());

                    otherAccount.setArguments(b);

                    ((CommanFragmentActitvty) activity).Loadfragment(otherAccount);
                } else {
                    Common.showToast(activity, "This is not vaild account");
                }


            }
        });

        holder.fname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (alllist.get(position).getUserid() != null) {
                    OtherAccount otherAccount = new OtherAccount();


                    Bundle b = new Bundle();
                    b.putString("userid", alllist.get(position).getUserid());

                    otherAccount.setArguments(b);

                    ((CommanFragmentActitvty) activity).Loadfragment(otherAccount);
                } else {
                    Common.showToast(activity, "This is not vaild account");
                }

            }
        });

        //   holder.btn_Fb.setOnClickListener(this);
        //  holder.btn_Snapchat.setOnClickListener(this);
        //   holder.btn_Twitter.setOnClickListener(this);
        //  holder.btn_Instagram.setOnClickListener(this);


    }


    @Override
    public int getItemCount() {
        return alllist.size();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.img_fb:

                if (isFB == false) {
                    isFB = true;

                    vholder.btn_Fb.setImageDrawable(activity.getResources().getDrawable(R.drawable.tick));

                } else {
                    isFB = false;
                    vholder.btn_Fb.setImageDrawable(activity.getResources().getDrawable(R.drawable.circle));
                }


                break;

            case R.id.img_Instagram:
                if (isInsta == false) {
                    isInsta = true;

                    vholder.btn_Instagram.setImageDrawable(activity.getResources().getDrawable(R.drawable.tick));

                } else {
                    isInsta = false;
                    vholder.btn_Instagram.setImageDrawable(activity.getResources().getDrawable(R.drawable.circle));
                }


                break;
            case R.id.img_Twitter:
                if (isTwitter == false) {
                    isTwitter = true;

                    vholder.btn_Twitter.setImageDrawable(activity.getResources().getDrawable(R.drawable.tick));

                } else {
                    isTwitter = false;
                    vholder.btn_Twitter.setImageDrawable(activity.getResources().getDrawable(R.drawable.circle));
                }

                break;
            case R.id.img_Snapchat:

                if (isSnap == false) {
                    isSnap = true;

                    vholder.btn_Snapchat.setImageDrawable(activity.getResources().getDrawable(R.drawable.tick));

                } else {
                    isSnap = false;
                    vholder.btn_Snapchat.setImageDrawable(activity.getResources().getDrawable(R.drawable.circle));
                }

                break;

        }

    }

    private void Public_Alert() {


        dialog = new Dialog(activity, R.style.DialogSlideAnim);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.setContentView(R.layout.alertcontact);
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        dialog.findViewById(R.id.OK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }


    // Filter Class
    public void filter(String charText) {
        alllist.clear();
        if (charText.length() == 0) {
            alllist.addAll(arraylist);
        } else {
            for (SearchResult.Detail wp : arraylist) {
                if (wp.getUsername()
                        .contains(charText) || wp.getFirst_name()
                        .contains(charText) || wp.getLast_name()
                        .contains(charText)) {
                    alllist.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        private LinearLayout ll_browsers;
        private RegularTextView Add_to_Contacts;
        private SemiBoldTextView username, fname;
        private ImageView imageView, btn_Fb, btn_Snapchat, btn_Twitter, btn_Instagram;


        public ViewHolder(View itemView) {
            super(itemView);
            username = (SemiBoldTextView) itemView.findViewById(R.id.username);
            fname = (SemiBoldTextView) itemView.findViewById(R.id.fname);
            imageView = (ImageView) itemView.findViewById(R.id.img_Dave_Smith);
            ll_browsers = (LinearLayout) itemView.findViewById(R.id.ll_browsers);

            btn_Fb = (ImageView) itemView.findViewById(R.id.img_fb);
            btn_Snapchat = (ImageView) itemView.findViewById(R.id.img_Snapchat);
            btn_Twitter = (ImageView) itemView.findViewById(R.id.img_Twitter);
            btn_Instagram = (ImageView) itemView.findViewById(R.id.img_Instagram);

            Add_to_Contacts = (RegularTextView) itemView.findViewById(R.id.Add_to_Contacts);
        }
    }


    private void ShowContactSaveDialog(final ArrayList<SearchResult.Detail> alllist, final int position) {


        AlertDialog myAlertDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Alert");
        builder.setMessage("Do you want to add " + alllist.get(position).getFirst_name() + " " + alllist.get(position).getLast_name() + " in your contacts?");
        builder.setPositiveButton("Yes ", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.dismiss();


                // DowonLoadVCF(alllist, position);

                Intent intent = new Intent(Intent.ACTION_INSERT);
                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);

                intent.putExtra(ContactsContract.Intents.Insert.NAME, alllist.get(position).getFirst_name() + " " + alllist.get(position).getLast_name());
                intent.putExtra(ContactsContract.Intents.Insert.PHONE, alllist.get(position).getMobile());
                intent.putExtra(ContactsContract.Intents.Insert.EMAIL, alllist.get(position).getEmail());

                activity.startActivity(intent);

            }
        });

        builder.setNegativeButton("No ", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.dismiss();


            }
        });

        builder.setCancelable(false);
        myAlertDialog = builder.create();
        myAlertDialog.show();

    }


    private void DowonLoadVCF(final ArrayList<SearchResult.Detail> alllist, final int position) {

        File vcfFile = new File(activity.getExternalFilesDir(null), "somerge_+" + alllist.get(position).getUsername() + ".vcf");
        FileWriter fw = null;
        try {
            fw = new FileWriter(vcfFile);
            fw.write("BEGIN:VCARD\r\n");
            fw.write("VERSION:3.0\r\n");
            fw.write("N:" + alllist.get(position).getUsername() + "\r\n");
            fw.write("FN:" + alllist.get(position).getFirst_name() + " " + alllist.get(position).getLast_name() + "\r\n");
            // fw.write("ORG:" + p.getCompanyName() + "\r\n");
            fw.write("TEL;TYPE=WORK,VOICE:" + alllist.get(position).getMobile() + "\r\n");
            fw.write("TEL;TYPE=HOME,VOICE:" + alllist.get(position).getMobile() + "\r\n");
            // fw.write("ADR;TYPE=WORK:;;" + p.getStreet() + ";" + p.getCity() + ";" + p.getState() + ";" + p.getPostcode() + ";" + p.getCountry() + "\r\n");
            fw.write("EMAIL;TYPE=PREF,INTERNET:" + alllist.get(position).getEmail() + "\r\n");
            fw.write("END:VCARD\r\n");
            fw.close();

            Intent i = new Intent();
            i.setAction(Intent.ACTION_VIEW);
            i.setDataAndType(Uri.fromFile(vcfFile), "text/x-vcard");
            activity.startActivity(i);


        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
