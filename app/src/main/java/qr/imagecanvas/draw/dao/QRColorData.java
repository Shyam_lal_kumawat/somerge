package qr.imagecanvas.draw.dao;

/**
 * Created by user7 on 24/11/16.
 */

public class QRColorData {
    int color;
    QRColorUnitData qrColorUnit1;
    QRColorUnitData qrColorUnit2;
    QRColorUnitData qrColorUnit3;
    QRColorUnitData qrColorUnit4;
    QRColorUnitData qrColorUnit5;

    public QRColorData(int color) {
        this.color = color;
    }

    public QRColorUnitData getQrColorUnit1() {
        return qrColorUnit1;
    }

    public void setQrColorUnit1(QRColorUnitData qrColorUnit1) {
        this.qrColorUnit1 = qrColorUnit1;
    }

    public QRColorUnitData getQrColorUnit2() {
        return qrColorUnit2;
    }

    public void setQrColorUnit2(QRColorUnitData qrColorUnit2) {
        this.qrColorUnit2 = qrColorUnit2;
    }

    public QRColorUnitData getQrColorUnit3() {
        return qrColorUnit3;
    }

    public void setQrColorUnit3(QRColorUnitData qrColorUnit3) {
        this.qrColorUnit3 = qrColorUnit3;
    }

    public QRColorUnitData getQrColorUnit4() {
        return qrColorUnit4;
    }

    public void setQrColorUnit4(QRColorUnitData qrColorUnit4) {
        this.qrColorUnit4 = qrColorUnit4;
    }

    public QRColorUnitData getQrColorUnit5() {
        return qrColorUnit5;
    }

    public void setQrColorUnit5(QRColorUnitData qrColorUnit5) {
        this.qrColorUnit5 = qrColorUnit5;
    }
}
