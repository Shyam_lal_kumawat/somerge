package qr.imagecanvas.draw.dao;

/**
 * Created by user7 on 24/11/16.
 */

public class QRColorUnitData {

    public int         startX;
    public int         startY;
    public int         height;
    public int         width;

    public QRColorUnitData(int startX, int startY, int height, int width) {
        this.startX = startX;
        this.startY = startY;
        this.height = height;
        this.width = width;
    }
}
