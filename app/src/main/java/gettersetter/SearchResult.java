package gettersetter;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by user47 on 19/10/16.
 */
public class SearchResult implements Serializable {


    public ArrayList<Detail> getDetail() {
        return detail;
    }

    public void setDetail(ArrayList<Detail> detail) {
        this.detail = detail;
    }

    public  ArrayList<Detail> detail;

    public class Detail {

        private String userid;
        private String name;
        private String email;
        private String username;
        private String password;
        private String mobile;
        private String facebook_id;
        private String image;
        private String thumb_image;
        private String last_login;
        private String step_completed;
        private String first_name;
        private String last_name;
        private String google_id;
        private String instagram_id;
        private String twitter_id;
        private String snapchat_id;
        private String is_private;
        private String dtdate;


        private String facebook;

        public String getInstagram() {
            return instagram;
        }

        public void setInstagram(String instagram) {
            this.instagram = instagram;
        }

        public String getSnapchat() {
            return snapchat;
        }

        public void setSnapchat(String snapchat) {
            this.snapchat = snapchat;
        }

        public String getTwitter() {
            return twitter;
        }

        public void setTwitter(String twitter) {
            this.twitter = twitter;
        }

        public String getFacebook() {
            return facebook;
        }

        public void setFacebook(String facebook) {
            this.facebook = facebook;
        }

        private String instagram;
        private String twitter;
        private String snapchat;



        public String getGenerated_image() {
            return generated_image;
        }

        public void setGenerated_image(String generated_image) {
            this.generated_image = generated_image;
        }

        private String generated_image;

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getFacebook_id() {
            return facebook_id;
        }

        public void setFacebook_id(String facebook_id) {
            this.facebook_id = facebook_id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getThumb_image() {
            return thumb_image;
        }

        public void setThumb_image(String thumb_image) {
            this.thumb_image = thumb_image;
        }

        public String getLast_login() {
            return last_login;
        }

        public void setLast_login(String last_login) {
            this.last_login = last_login;
        }

        public String getStep_completed() {
            return step_completed;
        }

        public void setStep_completed(String step_completed) {
            this.step_completed = step_completed;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getGoogle_id() {
            return google_id;
        }

        public void setGoogle_id(String google_id) {
            this.google_id = google_id;
        }

        public String getInstagram_id() {
            return instagram_id;
        }

        public void setInstagram_id(String instagram_id) {
            this.instagram_id = instagram_id;
        }

        public String getTwitter_id() {
            return twitter_id;
        }

        public void setTwitter_id(String twitter_id) {
            this.twitter_id = twitter_id;
        }

        public String getSnapchat_id() {
            return snapchat_id;
        }

        public void setSnapchat_id(String snapchat_id) {
            this.snapchat_id = snapchat_id;
        }

        public String getIs_private() {
            return is_private;
        }

        public void setIs_private(String is_private) {
            this.is_private = is_private;
        }

        public String getDtdate() {
            return dtdate;
        }

        public void setDtdate(String dtdate) {
            this.dtdate = dtdate;
        }


    }


}
