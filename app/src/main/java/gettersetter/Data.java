package gettersetter;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by user47 on 19/10/16.
 */
public class Data implements Serializable {

    private String userid;
    private String name;
    private String email;
    private String username;
    private String password;
    private String mobile;
    private String facebook_id;
    private String image;
    private String thumb_image;
    private String status;
    private String last_login;
    private String step_completed;

    public String getGenerated_image() {
        return generated_image;
    }

    public void setGenerated_image(String generated_image) {
        this.generated_image = generated_image;
    }

    public String getQr_image() {
        return qr_image;
    }

    public void setQr_image(String qr_image) {
        this.qr_image = qr_image;
    }

    private String generated_image;
    private String qr_image;



    public ArrayList<Social> social;

    public String getStep_completed() {
        return step_completed;
    }

    public void setStep_completed(String step_completed) {
        this.step_completed = step_completed;
    }

    public ArrayList<Social> getSocial() {
        return social;
    }

    public void setSocial(ArrayList<Social> social) {
        this.social = social;
    }


    public class Social {
        private String social_id;
        private String social_site;
        private String status;

        public String getSocial_id() {
            return social_id;
        }

        public void setSocial_id(String social_id) {
            this.social_id = social_id;
        }

        public String getSocial_site() {
            return social_site;
        }

        public void setSocial_site(String social_site) {
            this.social_site = social_site;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }


    }


    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getGoogle_id() {
        return google_id;
    }

    public void setGoogle_id(String google_id) {
        this.google_id = google_id;
    }

    public String getInstagram_id() {
        return instagram_id;
    }

    public void setInstagram_id(String instagram_id) {
        this.instagram_id = instagram_id;
    }

    public String getTwitter_id() {
        return twitter_id;
    }

    public void setTwitter_id(String twitter_id) {
        this.twitter_id = twitter_id;
    }

    public String getSnapchat_id() {
        return snapchat_id;
    }

    public void setSnapchat_id(String snapchat_id) {
        this.snapchat_id = snapchat_id;
    }

    public String getIs_private() {
        return is_private;
    }

    public void setIs_private(String is_private) {
        this.is_private = is_private;
    }

    private String first_name;
    private String last_name;
    private String google_id;
    private String instagram_id;
    private String twitter_id;
    private String snapchat_id;
    private String is_private;

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getSnapchat() {
        return snapchat;
    }

    public void setSnapchat(String snapchat) {
        this.snapchat = snapchat;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    private String instagram;
    private String twitter;
    private String snapchat;
    private String facebook;


    public String getDtdate() {
        return dtdate;
    }

    public void setDtdate(String dtdate) {
        this.dtdate = dtdate;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    private String dtdate;
}
